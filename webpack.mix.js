let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// Frontend Assets
mix.sass('resources/assets/sass/app.scss', 'public/css');

mix.copy('resources/assets/images/logo.png', 'public/images/logo.png');

mix.scripts([
    'resources/assets/js/front/navigation.js',
    'resources/assets/js/front/owlcarousel/owl.carousel.min.js'
], 'public/js/front/app.js');

// Admin Assets
mix.sass('resources/assets/sass/admin.scss', 'public/css')
    .copy('resources/assets/css/semantic.min.css', 'public/css/admin/semantic.min.css')
    .copyDirectory('resources/assets/css/themes', 'public/css/admin/themes')
    .copy('resources/assets/js/admin/require.min.js', 'public/js/admin/require.min.js')
    .copyDirectory('resources/assets/js/admin/vendors', 'public/js/admin/vendors')
    .copy('resources/assets/js/admin/dashboard.js', 'public/js/admin/dashboard.js')
    .babel('resources/assets/js/admin/core.js', 'public/js/admin/core.js');