<?php

namespace App\Console\Commands;

use Intervention\Image\ImageManagerStatic as Image;
use Intervention\Image\Exception\NotReadableException;
use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;

class DownloadProductImages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'download:images';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Download Product Images and store them in the DB';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::table('products')->where('cover', 'like', 'http%')->orderBy('id')->limit(1)->chunk(500, function ($products) {
            foreach ($products as $product) {

                $image = $product->cover;

                $filename = strtolower($product->name);
                $filename = str_replace(' ', '-', $filename);
                $filename = str_replace('/', '', $filename);
                $filename = str_replace('&', '', $filename);
                $filename = str_replace('!', '', $filename);
                $filename = str_replace('@', '', $filename);
                $filename = str_replace('$', 'usd', $filename);
                $filename = str_replace('&', '', $filename);
                $filename = str_replace('%', '', $filename);
                $filename = str_replace('(', '', $filename);
                $filename = str_replace(')', '', $filename);
                $filename = str_replace('{', '', $filename);
                $filename = str_replace('}', '', $filename);
                $filename = str_replace('[', '', $filename);
                $filename = str_replace(']', '', $filename);
                $filename = str_replace(':', '', $filename);
                $filename = str_replace(';', '', $filename);
                $filename = str_replace('"', '', $filename);
                $filename = str_replace('\'', '', $filename);
                $filename = str_replace('|', '', $filename);
                $filename = str_replace('?', '', $filename);
                $filename = str_replace('>', '', $filename);
                $filename = str_replace('.', '', $filename);
                $filename = str_replace(',', '', $filename);
                $filename = str_replace('~', '', $filename);
                $filename = str_replace('`', '', $filename);
                $filename = str_replace('=', '', $filename);
                $filename = str_replace('+', '', $filename);
                $filename = str_replace('---', '-', $filename);
                $filename = str_replace('--', '-', $filename);
                $filename = $filename . '-' . $product->id;

                $this->info('Downloading ' . $filename);
                $main = $this->output->createProgressBar(10);

                $ext = pathinfo($image, PATHINFO_EXTENSION);

                $path = 'storage/app/public/products/' . $filename . '.' . $ext;

                $db_path = 'products/' . $filename . '.' . $ext;

                try {
                    Image::make($image)->save($path);

                    DB::table('products')->where('id', $product->id)->update(['cover' => $db_path]);
                }
                catch(NotReadableException $e)
                {
                    // If error, stop and continue looping to next iteration

                    $not_found = 'storage/app/public/products/default-image.jpg';

                    DB::table('products')->where('id', $product->id)->update(['cover' => $not_found]);

                    continue;
                }

                $main->finish();
                $this->info(' Done.');
            }
        });

    }
}
