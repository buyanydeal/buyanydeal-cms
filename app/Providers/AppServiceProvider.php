<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        view()->composer('layouts.front.includes.header', function($view){
            $categories = \App\Cms\Categories\Models\Category::where('parent_id', NULL)->with('children')->has('products')->get()->sortBy('sort_order');
            $view->with(compact('categories'));
        });
    }
}
