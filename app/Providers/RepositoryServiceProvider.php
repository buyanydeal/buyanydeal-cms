<?php

namespace App\Providers;

use App\Cms\Brands\Repositories\BrandRepository;
use App\Cms\Brands\Interfaces\BrandRepositoryInterface;
use App\Cms\Categories\Repositories\CategoryRepository;
use App\Cms\Categories\Interfaces\CategoryRepositoryInterface;
use App\Cms\Products\Repositories\ProductRepository;
use App\Cms\Products\Interfaces\ProductRepositoryInterface;
use App\Cms\Scrapers\Repositories\ScraperCollectionRepository;
use App\Cms\Scrapers\Interfaces\ScraperCollectionRepositoryInterface;
use App\Cms\Shops\Repositories\ShopRepository;
use App\Cms\Shops\Interfaces\ShopRepositoryInterface;
use App\Cms\Sliders\Repositories\SliderRepository;
use App\Cms\Sliders\Interfaces\SliderRepositoryInterface;
use App\Cms\Users\Repositories\UserRepository;
use App\Cms\Users\Interfaces\UserRepositoryInterface;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(
            BrandRepositoryInterface::class,
            BrandRepository::class
        );
        $this->app->bind(
            CategoryRepositoryInterface::class,
            CategoryRepository::class
        );
        $this->app->bind(
            ProductRepositoryInterface::class,
            ProductRepository::class
        );
        $this->app->bind(
            ScraperCollectionRepositoryInterface::class,
            ScraperCollectionRepository::class
        );
        $this->app->bind(
            ShopRepositoryInterface::class,
            ShopRepository::class
        );
        $this->app->bind(
            SliderRepositoryInterface::class,
            SliderRepository::class
        );
        $this->app->bind(
            UserRepositoryInterface::class,
            UserRepository::class
        );
    }
}
