<?php
/**
 * Created by PhpStorm.
 * User: ruudvanengelenhoven
 * Date: 07/06/2018
 * Time: 10:14
 */

namespace App\Http\Controllers\Admin\Scraper;

use App\Cms\Categories\Models\Category;
use App\Cms\Scrapers\Interfaces\ScraperCollectionRepositoryInterface;
use App\Cms\Scrapers\Requests\CreateScraperCollectionRequest;
use App\Cms\Shops\Models\Shop;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;

class ScraperCollectionController extends Controller
{
    /**
     * @var ScraperCollectionRepositoryInterface
     */
    private $scraperCollectionRepo;

    /**
     * ShopController constructor.
     * @param ScraperCollectionRepositoryInterface $scraperCollectionRepo
     */
    public function __construct(ScraperCollectionRepositoryInterface $scraperCollectionRepo)
    {
        $this->scraperCollectionRepo = $scraperCollectionRepo;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin.scraper.collection.index',
            ['collections' => $this->scraperCollectionRepo->listScraperCollections('id', 'asc')]
        );
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $shops = Shop::get()->sortBy('name');
        $categories = Category::where('parent_id', NULL)->with('kids')->get()->sortBy('sort_order');

        return view('admin.scraper.collection.create', [
            'categories' => $categories,
            'shops' => $shops,
        ]);
    }

    public function store(CreateScraperCollectionRequest $request)
    {
        $request['categories'] = collect($request['categories'])->implode(',');
        $data = $request->except('_token', '_method');

        $this->scraperCollectionRepo->createScraperCollection($data);

        $request->session()->flash('message', 'Scraper Collection is successfully created');
        return redirect()->route('admin.scrapercollection.index');
    }

    public function destroy(int $id)
    {
        $scraper = $this->scraperCollectionRepo->findScraperCollectionById($id);
        $scraper->delete();

        request()->session()->flash('message', 'Delete successful');
        return redirect()->route('admin.scrapercollection.index');
    }
}