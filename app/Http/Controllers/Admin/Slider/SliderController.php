<?php
/**
 * Created by PhpStorm.
 * User: ruudvanengelenhoven
 * Date: 07/06/2018
 * Time: 10:14
 */

namespace App\Http\Controllers\Admin\Slider;

use App\Cms\Sliders\Interfaces\SliderRepositoryInterface;
use App\Cms\Sliders\Requests\CreateSliderRequest;
use App\Cms\Sliders\Requests\UpdateSliderRequest;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;

class SliderController extends Controller
{
    private $slider;

    public function __construct(SliderRepositoryInterface $slider)
    {
        $this->slider = $slider;
    }

    public function index()
    {
        return view('admin.slider.index',
            ['sliders' => $this->slider->listSliders('id', 'asc')]);
    }

    public function create()
    {
        return view('admin.slider.create');
    }

    public function store(CreateSliderRequest $request)
    {
        $data = $request->except('_token', '_method');

        if ($request->hasFile('image_desktop') && $request->file('image_desktop') instanceof UploadedFile) {
            $data['image_desktop'] = $this->slider->saveImage($request->file('image_desktop'));
        }

        if ($request->hasFile('image_mobile') && $request->file('image_mobile') instanceof UploadedFile) {
            $data['image_mobile'] = $this->slider->saveImage($request->file('image_mobile'));
        }

        $this->slider->createSlider($data);

        $request->session()->flash('message', 'Slider is successfully created');

        return redirect()->route('admin.slider.index');
    }

    public function edit($id)
    {
        return view('admin.slider.edit', ['slider' => $this->slider->findSliderById($id)]);
    }

    public function update(UpdateSliderRequest $request, int $id)
    {
        $slider = $this->slider->findSliderById($id);

        $data = $request->except('_token', '_method');

        if ($request->hasFile('image_desktop') && $request->file('image_desktop') instanceof UploadedFile) {
            $data['image_desktop'] = $this->slider->saveImage($request->file('image_desktop'));
        }

        if ($request->hasFile('image_mobile') && $request->file('image_mobile') instanceof UploadedFile) {
            $data['image_mobile'] = $this->slider->saveImage($request->file('image_mobile'));
        }

        $this->slider->updateSlider($data, $id);

        $request->session()->flash('message', 'Slider is successfully updated');

        return redirect()->route('admin.slider.edit', $id);
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(int $id)
    {
        $this->slider->findSliderById($id)->delete();

        request()->session()->flash('message', 'Slider is successfully deleted');

        return redirect()->route('admin.slider.index');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function removeImageDesktop(Request $request)
    {
        $this->slider->deleteFileDesktop($request->only('slider', 'image_desktop'), 'uploads');

        request()->session()->flash('message', 'Image deleted successfully');

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function removeImageMobile(Request $request)
    {
        $this->slider->deleteFileMobile($request->only('slider', 'image_mobile'), 'uploads');

        request()->session()->flash('message', 'Image deleted successfully');

        return redirect()->back();
    }
}