<?php
/**
 * Created by PhpStorm.
 * User: ruudvanengelenhoven
 * Date: 22/06/2018
 * Time: 09:38
 */

namespace App\Http\Controllers\Admin\User;

use App\Cms\Users\Interfaces\UserRepositoryInterface;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class UserController extends Controller
{
    private $users;

    /**
     * UserController constructor.
     * @param UserRepositoryInterface $users
     */
    public function __construct(UserRepositoryInterface $users)
    {
        $this->users = $users;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $list = $this->users->listUsers('created_at', 'desc');

        return view('admin.user.index', [
            'users' => $this->users->paginateArrayResults($list)
        ]);
    }

    public function create()
    {
        return view('admin.user.create');
    }

    /**
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $user = $this->users->findUserById($id);
        return view('admin.user.edit', ['user' => $user]);
    }
}