<?php
/**
 * Created by PhpStorm.
 * User: ruudvanengelenhoven
 * Date: 09/06/2018
 * Time: 03:13
 */
namespace App\Http\Controllers\Front\Brand;

use App\Cms\Brands\Interfaces\BrandRepositoryInterface;
use App\Cms\Brands\Models\Brand;
use App\Cms\Brands\Repositories\BrandRepository;

use App\Cms\Products\Interfaces\ProductRepositoryInterface;

use App\Http\Controllers\Controller;

class BrandController extends Controller
{
	private $brands;

	private $products;

	/**
	 * ProductController constructor.
	 * @param BrandRepositoryInterface $brands
     * @param ProductRepositoryInterface $products
	 */
	public function __construct(
	    BrandRepositoryInterface $brands,
        ProductRepositoryInterface $products
    )
	{
		$this->brands = $brands;
        $this->products = $products;
	}

	public function index()
	{
		$sortedBrands = Brand::allByFirstCharacter();

		return view('front.brand.index', [
			'sortedBrands' => $sortedBrands
		]);
	}

	public function show(string $slug)
	{
		$brand = $this->brands->findBrandBySlug(['slug' => $slug]);

        $brandRepo = new BrandRepository($brand);
        $products = $brandRepo->findProducts();

		return view('front.brand.brand', [
            'brand' => $brand,
            'products' => $this->products->paginateArrayResults($products->all(), 64)
        ]);
	}
}
