<?php
/**
 * Created by PhpStorm.
 * User: ruud
 * Date: 8/13/18
 * Time: 10:11 PM
 */

namespace App\Http\Controllers\Front\Sitemap;

use App\Cms\Brands\Interfaces\BrandRepositoryInterface;
use App\Cms\Brands\Models\Brand;
use App\Cms\Categories\Interfaces\CategoryRepositoryInterface;
use App\Cms\Categories\Models\Category;
use App\Cms\Categories\Repositories\CategoryRepository;
use App\Cms\Products\Interfaces\ProductRepositoryInterface;
use App\Cms\Products\Models\Product;
use App\Cms\Shops\Interfaces\ShopRepositoryInterface;
use App\Cms\Shops\Models\Shop;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;

class SitemapController extends Controller
{
    private $brands;

    private $categories;

    private $products;

    private $shops;

    public function __construct(
        BrandRepositoryInterface $brands,
        CategoryRepositoryInterface $categories,
        ProductRepositoryInterface $products,
        ShopRepositoryInterface $shops
    )
    {
        $this->brands = $brands;
        $this->categories = $categories;
        $this->products = $products;
        $this->shops = $shops;
    }

    public function index()
    {
        $brands = Brand::all()->orderBy('created_at', 'desc')->first();

        return response()->view('front.sitemap.index', [
            'brand' => $brands,
        ])->header('Content-Type', 'text/xml');
    }

    public function categories()
    {
        $categories = Category::with('children')->has('products')->get()->sortBy('updated_at');

        return response()->view('front.sitemap.categories', [
            'categories' => $categories,
        ])->header('Content-Type', 'text/xml');
    }

    public function productindex()
    {
        $lastupdate = Product::orderBy('created_at', 'desc')->first();
        $categories = Category::where('parent_id', NULL)->with('children')->has('products')->get()->sortBy('sort_order');

        return response()->view('front.sitemap.productindex', [
            'lastupdate' => $lastupdate,
            'categories' => $categories,
        ])->header('Content-Type', 'text/xml');
    }

    public function products(string $slug)
    {
        $category = $this->categories->findCategoryBySlug(['slug' => $slug]);

        $categoryRepo = new CategoryRepository($category);
        $products = $categoryRepo->findProducts();

        return response()->view('front.sitemap.products', [
            'products' => $products,
        ])->header('Content-Type', 'text/xml');
    }

    public function brands()
    {
        $lastupdate = Brand::orderBy('created_at', 'desc')->first();

        $brands = Brand::all();

        return response()->view('front.sitemap.brands', [
            'lastupdate' => $lastupdate,
            'brands' => $brands,
        ])->header('Content-Type', 'text/xml');
    }

    public function shops()
    {
        $lastupdate = Brand::orderBy('created_at', 'desc')->first();

        $shops = Shop::all();

        return response()->view('front.sitemap.shops', [
            'lastupdate' => $lastupdate,
            'shops' => $shops,
        ])->header('Content-Type', 'text/xml');
    }

}