<?php
/**
 * Created by PhpStorm.
 * User: ruudvanengelenhoven
 * Date: 29/05/2018
 * Time: 19:48
 */

namespace App\Http\Controllers\Front\Product;

use App\Cms\Brands\Interfaces\BrandRepositoryInterface;

use App\Cms\Categories\Interfaces\CategoryRepositoryInterface;

use App\Cms\Products\Interfaces\ProductRepositoryInterface;
use App\Cms\Products\Models\Product;
use App\Cms\Products\Transformations\ProductTransformable;

use App\Http\Controllers\Controller;

use Illuminate\Support\Collection;

class ProductController extends Controller
{
    use ProductTransformable;

    private $brandRepo;
    private $categoryRepo;
    private $productRepo;

    public function __construct(
        BrandRepositoryInterface $brandRepository,
        CategoryRepositoryInterface $categoryRepository,
        ProductRepositoryInterface $productRepository
    ) {
        $this->brandRepo = $brandRepository;
        $this->categoryRepo = $categoryRepository;
        $this->productRepo = $productRepository;
    }

    /**
     * Find the category via the slug
     *
     * @param string $slug
     * @return \App\Cms\Cms\Models\CmsPage
     */
    public function getProduct(string $slug)
    {
        $product = $this->productRepo->findProductBySlug(['slug' => $slug]);

        $images = $product->images()->get();

        $shops = $product->productshop()->get();

        return view('front.product.page', [
            'product' => $product,
            'images' => $images,
            'shops' => $shops
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function search()
    {
        $products = $this->productRepo->listProducts();

        if (request()->has('q') && request()->input('q') != '') {
            $products = $this->productRepo->searchProduct(request()->input('q'));
        }

        return view('front.product.product-search', [
            'products' => $this->productRepo->paginateArrayResults($products->all(), 16)
        ]);
    }
}