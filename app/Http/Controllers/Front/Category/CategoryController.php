<?php
/**
 * Created by PhpStorm.
 * User: ruudvanengelenhoven
 * Date: 16/06/2018
 * Time: 17:49
 */

namespace App\Http\Controllers\Front\Category;

use App\Cms\Brands\Interfaces\BrandRepositoryInterface;
use App\Cms\Categories\Interfaces\CategoryRepositoryInterface;
use App\Cms\Categories\Repositories\CategoryRepository;
use App\Cms\Products\Interfaces\ProductRepositoryInterface;
use App\Cms\ProductShop\Interfaces\ProductShopRepositoryInterface;

use App\Http\Controllers\Controller;

class CategoryController extends Controller
{

    private $brandRepo;
    private $categoryRepo;
    private $productRepo;
    private $productShopRepo;

    /**
     * CategoryController constructor.
     * @param BrandRepositoryInterface $brandRepository
     * @param CategoryRepositoryInterface $categoryRepository
     * @param ProductRepositoryInterface $productRepository
     */
    public function __construct(
        BrandRepositoryInterface $brandRepository,
        CategoryRepositoryInterface $categoryRepository,
        ProductRepositoryInterface $productRepository
//        ProductShopRepositoryInterface $productShopRepository
    )
    {
        $this->brandRepo = $brandRepository;
        $this->categoryRepo = $categoryRepository;
        $this->productRepo = $productRepository;
//        $this->productShopRepo = $productShopRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('front.category.index');
    }

    /**
     * @param string $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCategory(string $slug)
    {
        $category = $this->categoryRepo->findCategoryBySlug(['slug' => $slug]);

        $categoryRepo = new CategoryRepository($category);
        $products = $categoryRepo->findProducts();

        if (!$products->isEmpty()) {
            return view('front.category.view', [
                'category' => $category,
                'products' => $this->productRepo->paginateArrayResults($products->all(), 64)
            ]);
        } else {
            $data['title'] = '404';
            $data['name'] = 'Page not found';
            return response()->view('errors.404',$data,404);
        }
    }
}