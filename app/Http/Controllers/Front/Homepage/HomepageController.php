<?php
/**
 * Created by PhpStorm.
 * User: ruudvanengelenhoven
 * Date: 08/06/2018
 * Time: 18:51
 */

namespace App\Http\Controllers\Front\Homepage;

use App\Cms\Brands\Interfaces\BrandRepositoryInterface;
use App\Cms\Categories\Models\Category;
use App\Cms\Products\Interfaces\ProductRepositoryInterface;
use App\Cms\Products\Models\Product;
use App\Cms\Sliders\Interfaces\SliderRepositoryInterface;

use App\Http\Controllers\Controller;

class HomepageController extends Controller
{
	private $brand;

    private $slider;

    private $products;

	public function __construct(
	    BrandRepositoryInterface $brands,
        ProductRepositoryInterface $products,
        SliderRepositoryInterface $slider
    )
	{
		$this->brand = $brands;
        $this->products = $products;
        $this->slider = $slider;
	}

	public function index()
	{
        $slider = $this->slider->listSliders();

        $widgetWomen = Product::whereHas('categories',
            function($query) {
                $query->where('categories.id', '=', 1);
            })->limit(10)->orderBy('products.id', 'desc')->get();

        $widgetMen = Product::whereHas('categories',
            function($query) {
                $query->where('categories.id', '=', 155);
            })->limit(10)->orderBy('products.id', 'desc')->get();

        $widgetBabyKids = Product::whereHas('categories',
            function($query) {
                $query->where('categories.id', '=', 260);
            })->limit(10)->orderBy('products.id', 'desc')->get();

				$widgetTv = Product::whereHas('categories',
            function($query) {
                $query->where('categories.id', '=', 593);
            })->limit(4)->orderBy('products.id', 'desc')->get();

		return view('front.homepage.index', [
		    'slider' => $slider,
            'womenProducts' => $widgetWomen,
            'menProducts' => $widgetMen,
            'babyKidsProducts' => $widgetBabyKids,
						'tvProducts' => $widgetTv,
        ]);
	}
}
