<?php
/**
 * Created by PhpStorm.
 * User: ruudvanengelenhoven
 * Date: 29/06/2018
 * Time: 12:38
 */

namespace App\Http\Controllers\Front\Shop;

use App\Cms\Shops\Interfaces\ShopRepositoryInterface;
use App\Cms\Shops\Models\Shop;
use App\Cms\Shops\Repositories\ShopRepository;

use App\Http\Controllers\Controller;

class ShopController extends Controller
{

    private $shopRepo;

    /**
     * ShopController constructor.
     * @param ShopRepositoryInterface $shopRepository
     */
    public function __construct(
        ShopRepositoryInterface $shopRepository
    )
    {
        $this->shopRepo = $shopRepository;
    }

    public function index()
    {
        $sortedShops = Shop::allByFirstCharacter();

        return view('front.shop.index', [
            'sortedShops' => $sortedShops
        ]);
    }

    public function show(string $slug)
    {
        $shop = $this->shopRepo->findShopBySlug(['slug' => $slug]);

        return view('front.shop.view', [
            'shop' => $shop
        ]);
    }
}

