<?php
/**
 * Created by PhpStorm.
 * User: ruudvanengelenhoven
 * Date: 22/06/2018
 * Time: 11:00
 */

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfNotAdmin
{
    public function handle($request, Closure $next, $guard = 'admin')
    {
        if (!auth()->guard($guard)->check()) {
            $request->session()->flash('error', 'You must be authorised to see this page');
            return redirect(route('admin.login'));
        }
        return $next($request);
    }
}
