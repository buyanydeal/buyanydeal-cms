<?php
/**
 * Created by PhpStorm.
 * User: ruudvanengelenhoven
 * Date: 24/06/2018
 * Time: 21:58
 */

namespace App\Cms\Sliders\Models;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'title',
        'subtitle',
        'description',
        'image_desktop',
        'image_mobile',
        'url',
        'status'
    ];
}