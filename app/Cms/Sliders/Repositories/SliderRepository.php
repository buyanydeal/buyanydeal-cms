<?php
/**
 * Created by PhpStorm.
 * User: ruudvanengelenhoven
 * Date: 24/06/2018
 * Time: 21:57
 */

namespace App\Cms\Sliders\Repositories;

use App\Cms\Base\Exceptions\InvalidArgumentException;
use App\Cms\Base\Exceptions\NotFoundException;
use App\Cms\Base\Repositories\BaseRepository;

use App\Cms\Sliders\Interfaces\SliderRepositoryInterface;
use App\Cms\Sliders\Models\Slider;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;

class SliderRepository extends BaseRepository implements SliderRepositoryInterface
{
    /**
     * SliderRepository constructor.
     * @param Slider $slider
     */
    public function __construct(Slider $slider)
    {
        parent::__construct($slider);
        $this->model = $slider;
    }

    /**
     * @param string $order
     * @param string $sort
     * @param array $columns
     * @return Collection
     */
    public function listSliders(string $order = 'id', string $sort = 'desc', array $columns = ['*']) : Collection
    {
        return $this->all($columns, $order, $sort);
    }

    /**
     * @param array $params
     * @return Slider
     */
    public function createSlider(array $params) : Slider
    {
        try {
            $slider = new Slider($params);
            $slider->save();
            return $slider;
        } catch (QueryException $e) {
            throw new InvalidArgumentException($e->getMessage());
        }
    }

    /**
     * @param array $params
     * @param int $id
     * @return bool
     */
    public function updateSlider(array $params, int $id) : bool
    {
        try {
            return $this->update($params, $id);
        } catch (QueryException $e) {
            throw new InvalidArgumentException($e->getMessage());
        }
    }

    /**
     * @param int $id
     * @return Slider
     */
    public function findSliderById(int $id) : Slider
    {
        try {
            return $this->findOneOrFail($id);
        } catch (ModelNotFoundException $e) {
            throw new NotFoundException($e->getMessage());
        }
    }

    /**
     * @param Slider $slider
     * @return bool
     * @throws \Exception
     */
    public function deleteSlider(Slider $slider) : bool
    {
        return $this->model->delete();
    }

    /**
     * @param array $file
     * @param null $disk
     * @return bool
     */
    public function deleteFileDesktop(array $file, $disk = null) : bool
    {
        return $this->update(['image_desktop' => null], $file['slider']);
    }

    /**
     * @param array $file
     * @param null $disk
     * @return bool
     */
    public function deleteFileMobile(array $file, $disk = null) : bool
    {
        return $this->update(['image_mobile' => null], $file['slider']);
    }

    /**
     * @param UploadedFile $file
     * @return string
     */
    public function saveImage(UploadedFile $file) : string
    {
        return $file->store('sliders', ['disk' => 'public']);
    }
}
