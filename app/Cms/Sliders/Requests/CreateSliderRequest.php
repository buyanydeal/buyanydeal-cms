<?php
/**
 * Created by PhpStorm.
 * User: ruudvanengelenhoven
 * Date: 24/06/2018
 * Time: 21:49
 */

namespace App\Cms\Sliders\Requests;

use App\Cms\Base\Requests\FormRequest;

class CreateSliderRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'image_desktop' => ['required', 'file', 'image:png,jpeg,jpg,gif'],
            'image_mobile' => ['required', 'file', 'image:png,jpeg,jpg,gif'],
            'url' => 'required',
            'status' => 'required|boolean'
        ];
    }
}