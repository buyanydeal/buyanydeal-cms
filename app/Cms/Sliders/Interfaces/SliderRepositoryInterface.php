<?php
/**
 * Created by PhpStorm.
 * User: ruudvanengelenhoven
 * Date: 24/06/2018
 * Time: 21:57
 */

namespace App\Cms\Sliders\Interfaces;

use App\Cms\Base\Interfaces\BaseRepositoryInterface;

use App\Cms\Sliders\Models\Slider;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;

interface SliderRepositoryInterface extends BaseRepositoryInterface
{
    public function listSliders(string $order = 'id', string $sort = 'desc', array $columns = ['*']) : Collection;

    public function createSlider(array $data) : Slider;

    public function updateSlider(array $params, int $id) : bool;

    public function findSliderById(int $id) : Slider;

    public function deleteSlider(Slider $slider) : bool;

    public function deleteFileDesktop(array $file, $disk = null) : bool;

    public function deleteFileMobile(array $file, $disk = null) : bool;

    public function saveImage(UploadedFile $file) : string;
}