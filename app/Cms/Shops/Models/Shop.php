<?php
/**
 * Created by PhpStorm.
 * User: ruudvanengelenhoven
 * Date: 06/06/2018
 * Time: 07:50
 */

namespace App\Cms\Shops\Models;

use App\Cms\Products\Models\Product;
use App\Cms\ProductShop\Models\ProductShop;

use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'logo',
        'url',
        'slug',
        'meta_title',
        'meta_description',
        'meta_keywords',
        'status'
    ];

    public $timestamps = false;

    public function productshops()
    {
        return $this->hasMany(ProductShop::class);
    }

    public function shop()
    {
        return $this->morphMany(ProductShop::class, 'shop');
    }

    public static function allByFirstCharacter()
    {
        $shops = Shop::orderBy('name', 'ASC')->get();

        return $shops->reduce(function($sortedShops, $shop) {
            $firstChar = $shop->name[0];

            if (!isset($sortedShops[$firstChar])) {
                $sortedShops[$firstChar] = [];
            }

            $sortedShops[$firstChar][] = $shop;

            return $sortedShops;
        }, []);
    }
}