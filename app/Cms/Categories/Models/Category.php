<?php
/**
 * Created by PhpStorm.
 * User: ruudvanengelenhoven
 * Date: 06/06/2018
 * Time: 07:50
 */

namespace App\Cms\Categories\Models;

use App\Cms\Products\Models\Product;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Sofa\Eloquence\Eloquence;

class Category extends Model
{
    use Eloquence;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'page_title',
        'description',
        'seo_description',
        'cover',
        'thumbnail',
        'icon',
        'slug',
        'meta_title',
        'meta_description',
        'meta_keywords',
        'parent_id',
        'sort_order',
        'status'
    ];

    protected $table = 'categories';

    protected $appends = [
        'parent'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

    public function parent()
    {
        return $this->belongsTo(static::class, 'parent_id')->has('products');
    }

    public function children()
    {
        return $this->hasMany(static::class, 'parent_id', 'id')->has('products');
    }

    public function kids()
    {
        return $this->hasMany(static::class, 'parent_id', 'id');
    }

    public function parentFilter()
    {
        return $this->belongsTo(static::class, 'parent_id');
    }

    public function childrenFilter()
    {
        return $this->hasMany(static::class, 'parent_id', 'id');
    }

    /**
     * @return mixed|string
     */
    public function getParentsNames() {
        if($this->parent) {
            return $this->parent->getParentsNames(). " > " . $this->name;
        } else {
            return $this->name;
        }
    }

    /**
     * @return mixed|string
     */
    public function getParentsNamesFilter() {
        if($this->parentFilter) {
            return $this->parentFilter->getParentsNamesFilter(). " > " . $this->name;
        } else {
            return $this->name;
        }
    }

    /**
     * @param string $term
     * @return Collection
     */
    public function searchCategory(string $term) : Collection
    {
        return self::search($term, ['name' => 10, 'slug' => 10])->get();
    }

    public function getDataForJstree()
    {
        $allCategory                 = self::all()->sortBy('sort_order');

        $arrayDataConvertedForJstree = null;

        foreach ($allCategory as $category) {

            if ($category['parent_id'] == 0) {
                $category['parent_id'] = "#";
            }

            $arrayDataConvertedForJstree[] = array(
                'id'     => $category['id'],
                'parent' => $category['parent_id'],
                'text'   => $category['name'],
            );
        }

        return $arrayDataConvertedForJstree;
    }
}