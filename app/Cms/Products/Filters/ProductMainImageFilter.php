<?php

namespace App\Cms\Products\Filters;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class ProductMainImageFilter implements FilterInterface
{
    public function applyFilter(Image $image)
    {
        return $image->fit(400, 400)->encode('jpg', 80);
    }
}