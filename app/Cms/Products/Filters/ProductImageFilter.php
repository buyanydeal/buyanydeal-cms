<?php

namespace App\Cms\Products\Filters;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class ProductImageFilter implements FilterInterface
{
    public function applyFilter(Image $image)
    {
        return $image->fit(180, 180)->encode('jpg', 80);
    }
}