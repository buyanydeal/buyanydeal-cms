<?php

namespace App\Cms\Products\Filters;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class ProductSmallImageFilter implements FilterInterface
{
    public function applyFilter(Image $image)
    {
        return $image->resize(80, 80)->encode('jpg', 70);
    }
}