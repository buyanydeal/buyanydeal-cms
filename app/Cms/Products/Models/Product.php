<?php
/**
 * Created by PhpStorm.
 * User: ruudvanengelenhoven
 * Date: 06/06/2018
 * Time: 07:39
 */

namespace App\Cms\Products\Models;

use App\Cms\Brands\Models\Brand;

use App\Cms\Categories\Models\Category;

use App\Cms\ProductImages\Models\ProductImage;

use App\Cms\Products\Transformations\ProductTransformable;

use App\Cms\ProductShop\Models\ProductShop;

use App\Cms\Shops\Models\Shop;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Sofa\Eloquence\Eloquence;

class Product extends Model
{
    use Eloquence;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'brand_id',
        'sku',
        'ean',
        'name',
        'description',
        'cover',
        'slug',
        'meta_title',
        'meta_description',
        'meta_keywords',
        'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public $transformer = ProductTransformable::class;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function images()
    {
        return $this->hasMany(ProductImage::class);
    }

    /**
     * @param string $term
     * @return Collection
     */
    public function searchProduct(string $term) : Collection
    {
        return self::search($term, ['name' => 8, 'brand.name' => 10, 'categories.name' => 10, 'slug' => 10])->get();
    }

    public function products()
    {
        return $this->morphMany(ProductShop::class, 'product');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function productshop()
    {
        return $this->hasMany(ProductShop::class);
    }

    public function shop()
    {
        return $this->hasManyThrough(
            ProductShop::class,Shop::class,
            'id', 'shop_id', 'id'
        );
    }

}