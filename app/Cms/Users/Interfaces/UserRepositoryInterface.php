<?php
/**
 * Created by PhpStorm.
 * User: ruudvanengelenhoven
 * Date: 23/06/2018
 * Time: 13:24
 */

namespace App\Cms\Users\Interfaces;

use App\Cms\Base\Interfaces\BaseRepositoryInterface;
use App\Cms\Users\Models\User;

use Illuminate\Support\Collection;

interface UserRepositoryInterface extends BaseRepositoryInterface
{
    public function listUsers(string $order = 'id', string $sort = 'desc') : array;

    public function findUserById(int $id) : User;
}