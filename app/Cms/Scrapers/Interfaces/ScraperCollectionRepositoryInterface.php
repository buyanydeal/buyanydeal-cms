<?php
/**
 * Created by PhpStorm.
 * User: ruudvanengelenhoven
 * Date: 06/06/2018
 * Time: 07:57
 */

namespace App\Cms\Scrapers\Interfaces;

use App\Cms\Base\Interfaces\BaseRepositoryInterface;
use App\Cms\Scrapers\Models\ScraperCollection;

use Illuminate\Support\Collection;

interface ScraperCollectionRepositoryInterface extends BaseRepositoryInterface
{
    public function listScraperCollections(string $order = 'id', string $sort = 'desc', array $columns = ['*']) : Collection;

    public function createScraperCollection(array $data) : ScraperCollection;

    public function updateScraperCollection(array $params, int $id) : bool;

    public function findScraperCollectionById(int $id) : ScraperCollection;

    public function deleteScraperCollection(ScraperCollection $scraperCollection) : bool;
}