<?php
/**
 * Created by PhpStorm.
 * User: ruudvanengelenhoven
 * Date: 07/06/2018
 * Time: 09:15
 */

namespace App\Cms\Scrapers\Requests;

use App\Cms\Base\Requests\FormRequest;

class CreateScraperCollectionRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'collection' => 'required',
            'name' => 'required',
            'shop_id' => 'required',
            'categories' => 'required',
        ];
    }
}
