<?php
/**
 * Created by PhpStorm.
 * User: ruudvanengelenhoven
 * Date: 06/06/2018
 * Time: 08:02
 */

namespace App\Cms\Scrapers\Repositories;

use App\Cms\Base\Exceptions\InvalidArgumentException;
use App\Cms\Base\Repositories\BaseRepository;

use App\Cms\Scrapers\Interfaces\ScraperCollectionRepositoryInterface;
use App\Cms\Scrapers\Models\ScraperCollection;

use Illuminate\Database\QueryException;
use Illuminate\Support\Collection;

class ScraperCollectionRepository extends BaseRepository implements ScraperCollectionRepositoryInterface
{
    public function __construct(ScraperCollection $scraperCollection)
    {
        parent::__construct($scraperCollection);
        $this->model = $scraperCollection;
    }

    public function listScraperCollections(string $order = 'id', string $sort = 'desc', array $columns = ['*']) : Collection
    {
        return $this->all($columns, $order, $sort);
    }

    public function createScraperCollection(array $params) : ScraperCollection
    {
        try {
            $scraperCollection = new ScraperCollection($params);
            $scraperCollection->save();

            return $scraperCollection;

        } catch (QueryException $e) {
            throw new InvalidArgumentException($e->getMessage());
        }
    }

    public function updateScraperCollection(array $params, int $id) : bool
    {
        try {
            return $this->update($params, $id);
        } catch (QueryException $e) {
            throw new InvalidArgumentException($e->getMessage());
        }
    }


    public function findScraperCollectionById(int $id) : ScraperCollection
    {
        try {
            return $this->findOneOrFail($id);
        } catch (ModelNotFoundException $e) {
            throw new NotFoundException($e->getMessage());
        }
    }

    public function deleteScraperCollection(ScraperCollection $scraperCollection) : bool
    {
        return $this->model->delete();
    }
}