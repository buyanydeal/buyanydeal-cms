<?php
/**
 * Created by PhpStorm.
 * User: ruudvanengelenhoven
 * Date: 06/06/2018
 * Time: 07:50
 */

namespace App\Cms\Scrapers\Models;

use App\Cms\Shops\Models\Shop;

use Illuminate\Database\Eloquent\Model;

class ScraperCollection extends Model
{
    protected $fillable = [
        'collection',
        'name',
        'shop_id',
        'categories'
    ];

    protected $table = 'scraper_collections';

    public function shop()
    {
        return $this->belongsTo(Shop::class);
    }
}