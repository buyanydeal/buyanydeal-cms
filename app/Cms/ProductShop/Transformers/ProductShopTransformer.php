<?php

namespace App\Cms\ProductShop\Transformers;

use App\Cms\ProductShop\Models\ProductShop;

use League\Fractal\TransformerAbstract;

class ProductShopTransformer extends TransformerAbstract
{
    public function transform(ProductShop $productshop)
    {
        $data = [
            'id' => $productshop->id()
        ];

        return $data;
    }
}