<?php

namespace App\Cms\ProductShop\Repositories;

use App\Cms\Base\Repositories\BaseRepository;
use App\Cms\Base\Exceptions\NotFoundException;
use App\Cms\ProductShop\Models\ProductShop;
use App\Cms\ProductShop\Interfaces\ProductShopRepositoryInterface;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;

class ProductShopRepository extends BaseRepository implements ProductShopRepositoryInterface
{
    /**
     * ProductAttributeRepository constructor.
     * @param ProductShop $productShop
     */
    public function __construct(ProductShop $productShop)
    {
        parent::__construct($productShop);
        $this->model = $productShop;
    }

    /**
     * @param int $id
     * @return mixed
     * @throws NotFoundException
     */
    public function findProductShopById(int $id)
    {
        try {
            return $this->findOneOrFail($id);
        } catch (ModelNotFoundException $e) {
            throw new NotFoundException($e);
        }
    }

    public function findProducts() : Collection
    {
        return $this->model->products;
    }

    /**
     * @return mixed
     */
//    public function findProduct() : Product
//    {
//        return $this->model->product;
//    }
}