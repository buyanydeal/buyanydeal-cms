<?php

namespace App\Cms\ProductShop\Interfaces;

use App\Cms\Base\Interfaces\BaseRepositoryInterface;

use Illuminate\Support\Collection;

interface ProductShopRepositoryInterface extends BaseRepositoryInterface
{
    public function findProductShopById(int $id);

    public function findProducts() : Collection;

}