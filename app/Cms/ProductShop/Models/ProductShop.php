<?php

namespace App\Cms\ProductShop\Models;

use App\Cms\Products\Models\Product;
use App\Cms\Shops\Models\Shop;

use Illuminate\Database\Eloquent\Model;

class ProductShop extends Model
{
    protected $fillable = [
        'original_price',
        'special_price',
        'delivery_time',
        'delivery_cost',
        'stock_status',
        'url'
    ];

    public function product()
    {
        return $this->hasManyThrough(
            Product::class, ProductShop::class,
            'shop_id', 'id', 'product_id'
        );
    }

    public function shop()
    {
        return $this->hasManyThrough(
            Shop::class, ProductShop::class,
            'product_id', 'id', 'shop_id'
        );
    }
}
