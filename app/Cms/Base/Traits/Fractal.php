<?php

namespace App\Cms\Base\Traits;

use Illuminate\Http\Response;

use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

trait Fractal
{
    protected $fractal;
    protected $statusCode = 200;

    protected function parseIncludes($includes = [])
    {
        if ($includes) {
            app()->fractal->parseIncludes($includes);
        }
        return $this;
    }

    protected function respondWithCollection($paginator, $callback, $meta = [])
    {
        if (app('request')->includes) {
            $this->parseIncludes(app('request')->includes);
        }

        if ($paginator instanceof \Illuminate\Pagination\LengthAwarePaginator) {
            $collection = $paginator->getCollection();
        } else {
            $collection = $paginator;
        }

        $resource = new Collection($collection, $callback);

        $meta = array_merge([
            'lang' => app()->getLocale()
        ], $meta);

        $resource->setMeta($meta);


        if ($paginator instanceof \Illuminate\Pagination\LengthAwarePaginator) {
            $resource->setPaginator(new IlluminatePaginatorAdapter($paginator));
        }

        $rootScope = app()->fractal->createData($resource);

        return $this->respondWithArray($rootScope->toArray());
    }

    protected function respondWithArray(array $array, array $headers = [])
    {
        return response($array, $this->statusCode)->withHeaders($headers);
    }
}