<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                'name' => 'roula',
                'email' => 'roulaa6@gmail.com',
                'password' => bcrypt('Good99.1!'),
                'created_at' => Carbon::now(),
            ]
        );
    }
}
