<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCollectionsToScraperUrlsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('scraper_urls', function (Blueprint $table) {
            $table->integer('collection_id')->unsigned()->index()->after('product_id');
            $table->foreign('collection_id')->nullable()->references('id')->on('scraper_collections')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('scraper_urls', function (Blueprint $table) {
            //
        });
    }
}
