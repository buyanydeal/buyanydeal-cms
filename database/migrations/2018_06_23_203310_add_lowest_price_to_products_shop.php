<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLowestPriceToProductsShop extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_shops', function (Blueprint $table) {
            $table->decimal('lowest_price')->nullable()->after('special_price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_shops', function (Blueprint $table) {
            $table->dropColumn('lowest_price');
        });
    }
}
