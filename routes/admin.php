<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::namespace('Admin')->group(function () {
    Route::namespace('User')->group(function () {
        Route::get('buymin/login', 'LoginController@showLoginForm')->name('admin.login');
        Route::post('buymin/login', 'LoginController@login')->name('admin.login');
        Route::get('buymin/logout', 'LoginController@logout')->name('admin.logout');
    });
});

Route::group([
    'prefix' => 'buymin',
    'middleware' => ['admin'],
    'as' => 'admin.'
], function () {
    Route::namespace('Admin')->group(function () {
        Route::namespace('Brand')->group(function () {
            Route::resource('brand', 'BrandController');
            Route::get('remove-image-brand', 'BrandController@removeImage')->name('brand.remove.image');
        });
        Route::namespace('Category')->group(function () {
            Route::resource('category', 'CategoryController');
            Route::get('remove-image-category', 'CategoryController@removeImage')->name('category.remove.image');
            Route::get('categories-tree', ['uses' => 'CategoryController@getTree', 'as' => 'category.getTree']);
        });
        Route::namespace('Dashboard')->group(function () {
            Route::get('/', 'DashboardController@index')->name('dashboard');
        });
        Route::namespace('Product')->group(function () {
            Route::resource('product', 'ProductController');
            Route::get('remove-image-product', 'ProductController@removeImage')->name('product.remove.image');
            Route::get('remove-image-thumb', 'ProductController@removeThumbnail')->name('product.remove.thumb');
        });
        Route::namespace('Scraper')->group(function () {
            Route::resource('scrapercollection', 'ScraperCollectionController');
        });
        Route::namespace('Shop')->group(function () {
            Route::resource('shop', 'ShopController');
            Route::get('remove-image-logo', 'ShopController@removeImage')->name('shop.remove.logo');
        });
        Route::namespace('Slider')->group(function () {
            Route::resource('slider', 'SliderController');
            Route::get('remove-image-image_desktop', 'SliderController@removeImageDesktop')->name('slider.remove.image_desktop');
            Route::get('remove-image-image_mobile', 'SliderController@removeImageMobile')->name('slider.remove.image_mobile');
        });
        Route::namespace('User')->group(function () {
            Route::resource('user', 'UserController');
        });
    });
});