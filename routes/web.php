<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Sitemaps
Route::namespace('Front')->group(function () {
    Route::namespace('Sitemap')->group(function () {
        Route::get('/sitemap/brands.xml', 'SitemapController@brands');
        Route::get('/sitemap/categories.xml', 'SitemapController@categories');
        Route::get('/sitemap/shops.xml', 'SitemapController@shops');
        Route::get('/sitemap/products.xml', 'SitemapController@productindex');
        Route::get('/sitemap/products-{slug}.xml', 'SitemapController@products');
    });
});

Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(),
        'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
    ], function() {

        // Homepage & Product Routes
        Route::namespace('Front')->group(function () {
            Route::namespace('Homepage')->group(function () {
                Route::get('/', 'HomepageController@index')->name('front.home.index');
            });
            Route::namespace('Product')->group(function () {
                Route::get('compare-prices/{slug}.html', 'ProductController@getProduct')->name('front.product.slug');
                Route::get("search", 'ProductController@search')->name('search.product');
            });
        });

        // Brand Routes
        Route::group([
            'prefix' => 'compare-brands',
            'as' => 'front.'
        ], function () {
            Route::namespace('Front')->group(function () {
                Route::namespace('Brand')->group(function () {
                    Route::get('/', 'BrandController@index')->name('brand.index');
                    Route::get('{brand}.html', 'BrandController@show')->name('get.brand');
                });
            });
        });

        // Category Routes
        Route::group([
            'prefix' => 'all-categories',
            'as' => 'front.'
        ], function () {
            Route::namespace('Front')->group(function () {
                Route::namespace('Category')->group(function () {
                    Route::get('/', 'CategoryController@index')->name('category.index');
                });
            });
        });

        Route::group([
            'as' => 'front.'
        ], function () {
            Route::namespace('Front')->group(function () {
                Route::namespace('Category')->group(function () {
                    Route::get('{slug}.html', 'CategoryController@getCategory')->name('category.view');
                });
            });
        });


        // Shop Routes
        Route::group([
            'prefix' => 'online-shopping-uae',
            'as' => 'front.'
        ], function () {
            Route::namespace('Front')->group(function () {
                Route::namespace('Shop')->group(function () {
                    Route::get('/', 'ShopController@index')->name('shop.index');;
                    Route::get('{slug}.html', 'ShopController@show')->name('shop.show');
                });
            });
        });
    });