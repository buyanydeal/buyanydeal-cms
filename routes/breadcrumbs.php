<?php
/**
 * Created by PhpStorm.
 * User: ruudvanengelenhoven
 * Date: 23/06/2018
 * Time: 16:10
 */

// Home
Breadcrumbs::for('front.home.index', function ($trail) {
    $trail->push('Home', route('front.home.index'));
});

// Home > Brands
Breadcrumbs::for('front.brand.index', function ($trail) {
    $trail->parent('front.home.index');
    $trail->push('Compare Brands', route('front.brand.index'));
});
Breadcrumbs::for('front.get.brand', function ($trail, $brand) {
    $trail->parent('front.brand.index');
    $trail->push($brand->name, route('front.get.brand', $brand->slug));
});

// Home > Products
Breadcrumbs::for('front.product.slug', function ($trail, $product) {
    foreach ($product->categories as $category) {
        $trail->push($category->name, route('front.category.view', $category->slug));
    }
    $trail->push($product->name, route('front.product.slug', $product->slug));
});

// Home > Shops
Breadcrumbs::for('front.shop.index', function ($trail) {
    $trail->parent('front.home.index');
    $trail->push('Compare Shops', route('front.shop.index'));
});
Breadcrumbs::for('front.get.shop', function ($trail, $shop) {
    $trail->parent('front.shop.index');
    $trail->push($shop->name, route('front.get.shop', $shop->id));
});