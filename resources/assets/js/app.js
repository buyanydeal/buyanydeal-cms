//Detect swipe on mobile
swipe_det = new Object();
swipe_det.sX = 0;
swipe_det.sY = 0;
swipe_det.eX = 0;
swipe_det.eY = 0;
var min_x = 100;  //min x swipe for horizontal swipe
var max_x = 140;  //max x difference for vertical swipe
var min_y = 40;  //min y swipe for vertical swipe
var max_y = 50;  //max y difference for horizontal swipe
var direc = "";

function detectswipe(state) {
    if(state){
        document.addEventListener('touchstart', myTouchStart, false);
        document.addEventListener('touchmove', myTouchMove, false);
        document.addEventListener('touchend', myTouchEnd, false);
    } else {
        document.removeEventListener('touchstart', myTouchStart, false);
        document.removeEventListener('touchmove', myTouchMove, false);
        document.removeEventListener('touchend', myTouchEnd, false);
    }
}

function myTouchStart(e){
    var t = e.touches[0];
    swipe_det.sX = t.screenX;
    swipe_det.sY = t.screenY;
}

function myTouchMove(e){
    //e.preventDefault();
    var t = e.touches[0];
    swipe_det.eX = t.screenX;
    swipe_det.eY = t.screenY;
}

function myTouchEnd(e){
    if(swipe_det.sX != swipe_det.eX && swipe_det.eX != 0 && swipe_det.eX < swipe_det.sX && Math.abs(swipe_det.eY - swipe_det.sY) < 60){
        toggleNav();
        swipe_det.eX = 0;
    }
}

function toggleNav(){
    if(document.body.classList.contains('open-nav')){
        document.body.classList.remove('open-nav');
        detectswipe(false);
    } else {
        document.body.classList.add('open-nav');
        detectswipe(true);
    }
}

// Mobile navigation toggle
var nav_toggle = document.getElementById('nav_toggle');

nav_toggle.addEventListener('click', function(e){
    toggleNav();
});

var nav_items = document.querySelectorAll('.nested-list-item');


var back_btn = document.createElement('a');
back_btn.innerHTML = "<i class=\"icon-back\"></i><span class=\"hide-content-text\">Back</span>";
back_btn.classList.add('hide-content-link');

back_btn.onclick = function(){
    this.parentElement.classList.remove('is-active');
};

require(["matchMedia"], function () {
    mediaCheck({
        media: '(max-width: 992px)',
        entry: function () {
            let links;
            for (let i = 0; i < nav_items.length; i++) {
                links = nav_items[i].getElementsByClassName('nav-anchor');

                if (links.length !== 1) {
                    links[0].classList.add("has-child");

                    links[0].addEventListener('click', function(e) {
                        e.preventDefault();

                        this.parentElement.getElementsByClassName('toggle-content')[0].classList.add('is-active');
                        this.parentElement.getElementsByClassName('toggle-content')[0].prepend(back_btn);
                    });
                }

            }
        }
    });
});