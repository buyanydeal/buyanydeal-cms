var mobileNav = document.getElementById('mobile__nav--toggle'),
    megaMenuToggle = document.getElementById('megaMenuToggle'),
    searchToggle = document.getElementById('search_toggle'),
    searchClose = document.getElementById('search_close'),
    menuClose = document.getElementById('catnav_close'),
    nav_items = document.querySelectorAll('.catmenu__item'),
    back_btn = document.createElement('a');

back_btn.innerHTML = "<i class=\"icon-back\"></i><span class=\"hide-content-text\">Back</span>";
back_btn.classList.add('hide-content-link');

back_btn.onclick = function(){
    this.parentElement.classList.remove('is-active');
};



function toggleMegaMenu() {
    if(document.body.classList.contains('megamenu-open')){
        document.body.classList.remove('megamenu-open');
    } else {
        document.body.classList.add('megamenu-open');
    }
}

function toggleSearch() {
    if(document.body.classList.contains('search-open')){
        document.body.classList.remove('search-open');
    } else {
        document.body.classList.add('search-open');
    }
}


mobileNav.addEventListener('click', function(e){
    toggleMegaMenu();
});

searchToggle.addEventListener('click', function(e){
    toggleSearch();
});

searchClose.addEventListener('click', function(e){
    toggleSearch();
});

menuClose.addEventListener('click', function(e){
    toggleMegaMenu();
});

megaMenuToggle.addEventListener('click', function(e){
    toggleMegaMenu();
});

var links;

for (var i = 0; i < nav_items.length; i++) {
    links = nav_items[i].getElementsByClassName('catmenu__item--link');
    if (links.length !== 1) {
        links[0].classList.add("has-child");

        links[0].addEventListener('click', function(e) {
            e.preventDefault();

            this.parentElement.getElementsByClassName('toggle-content')[0].classList.add('is-active');
            this.parentElement.getElementsByClassName('toggle-content')[0].prepend(back_btn);
        });
    }
}