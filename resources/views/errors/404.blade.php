@extends('layouts.front.app',[
    'pageTitle' => '404 Page Not Found | Buyanydeal',
    'metaDescription' => 'Compare prices in the online shopping mall in the UAE. Buyanydeal is makes online shopping fun and cheaper. Read reviews and compare.',
    'metaKeywords' => 'online shopping, comparison, uae, dubai, Abu Dhabi, Al Ain, Sharjah, buy online, cheapest price',
    'canonical' => URL::current()
])



@section('content')



    <h1 class="h4 mb-3">Sorry, we couldn't find the page your were looking for</h1>
    <p>Our bad. Maybe the page you were looking for doesn't exist anymore.</p>

@endsection

@section('footerjs')
    <script>
        var loadDeferredStyles = function() {
            var addStylesNode = document.getElementById("deferred-styles");
            var replacement = document.createElement("div");
            replacement.innerHTML = addStylesNode.textContent;
            document.body.appendChild(replacement)
            addStylesNode.parentElement.removeChild(addStylesNode);
        };
        var raf = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
            window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
        if (raf) raf(function() { window.setTimeout(loadDeferredStyles, 0); });
        else window.addEventListener('load', loadDeferredStyles);
    </script>
@endsection