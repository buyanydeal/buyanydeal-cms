@extends('layouts.admin.app')

@section('content')

    @include('layouts.errors-and-messages')

    <form action="{{ route('admin.slider.store') }}" method="post" class="card" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="card-body">
            <h3 class="card-title">Add new Slider</h3>
            @include('layouts.shared.fields.input', [
                'title' => 'Slider Name',
                'name' => 'name',
                'type' => 'text',
                'required' => 'true'
                ])
            @include('layouts.shared.fields.input', [
                'title' => 'Slider Title',
                'name' => 'title',
                'type' => 'text',
                'required' => 'false'
                ])
            @include('layouts.shared.fields.input', [
                'title' => 'Slider Subtitle',
                'name' => 'subtitle',
                'type' => 'text',
                'required' => 'false'
                ])
            @include('layouts.shared.fields.textarea', [
                'title' => 'Slider Description',
                'name' => 'description',
                'ckeditor' => 'true',
                'required' => 'false'
                ])
            @include('layouts.shared.fields.input', [
                'title' => 'Image Desktop',
                'name' => 'image_desktop',
                'type' => 'file',
                'required' => 'false'
                ])
            @include('layouts.shared.fields.input', [
                'title' => 'Image Mobile',
                'name' => 'image_mobile',
                'type' => 'file',
                'required' => 'false'
                ])
            @include('layouts.shared.fields.input', [
                'title' => 'URL Key',
                'name' => 'url',
                'type' => 'text',
                'required' => 'true'
                ])

            <div class="form-group">
                <label for="status" class="form-label">Status</label>
                <select id="status" name="status" class="form-control custom-select">
                    <option value="1">Enabled</option>
                    <option value="0">Disabled</option>
                </select>
            </div>
        </div>
        <div class="card-footer text-right">
            <div class="d-flex">
                <a href="{{ route('admin.slider.index') }}" class="btn btn-link">Cancel</a>
                <button type="submit" class="btn btn-primary ml-auto">Add Shop</button>
            </div>
        </div>
    </form>

@endsection

@section('footerjs')
    <script src="{{ asset('//cdn.ckeditor.com/4.8.0/standard/ckeditor.js') }}"></script>
@endsection
