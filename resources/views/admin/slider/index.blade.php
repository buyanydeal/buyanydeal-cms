@extends('layouts.admin.app')

@section('content')

    @include('layouts.errors-and-messages')

    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Manage Sliders</h3>
            <div class="card-options">
                <a href="{{ route('admin.slider.create') }}" class="btn btn-outline-primary btn-sm float-right"><i class="fe fe-plus mr-2"></i>Add Slider</a>
            </div>
        </div>
        <div class="card-body table-responsive">
            @if($sliders)
                <table class="table card-table table-vcenter">
                    <thead>
                    <tr>
                        <th>id</th>
                        <th>Name</th>
                        <th>Image Desktop</th>
                        <th>Image Mobile</th>
                        <th>Status</th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($sliders as $slider)
                        <tr>
                            <td>{{ $slider->id }}</td>
                            <td>{{ $slider->name }}</td>
                            <td><img src="{{ asset("storage/$slider->image_desktop") }}" alt="" class="img-responsive" width="300"></td>
                            <td><img src="{{ asset("storage/$slider->image_mobile") }}" alt="" class="img-responsive" width="200"></td>
                            <td>
                                @if($slider->status == 1)
                                    <span class="status-icon bg-success"></span>
                                @else
                                    <span class="status-icon bg-danger"></span>
                                @endif
                            </td>
                            <td class="w-1">
                                <form action="{{ route('admin.slider.destroy', $slider->id) }}" method="post">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="delete">
                                    <div class="btn-group">
                                        <button onclick="return confirm('Are you sure?')" type="submit" class="btn icon" style="padding:0; background:transparent">
                                            <i class="fe fe-trash"></i>
                                        </button>
                                    </div>
                                </form>
                            </td>
                            <td class="w-1">
                                <a class="icon" href="{{ route('admin.slider.edit', $slider->id) }}">
                                    <i class="fe fe-edit"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>

@endsection
