@extends('layouts.admin.app')

@section('content')

    @include('layouts.errors-and-messages')

    <form action="{{ route('admin.slider.update', $slider->id) }}" method="post" class="card" enctype="multipart/form-data">
        <input type="hidden" name="_method" value="put">
        {{ csrf_field() }}
        <div class="card-body">
            <h3 class="card-title">Edit Language</h3>
            @include('layouts.shared.fields.input', [
                'title' => 'Slider Name',
                'name' => 'name',
                'type' => 'text',
                'value' => $slider->name,
                'required' => 'true'
                ])
            @include('layouts.shared.fields.input', [
                'title' => 'Slider Title',
                'name' => 'title',
                'type' => 'text',
                'value' => $slider->title,
                'required' => 'false'
                ])
            @include('layouts.shared.fields.input', [
                'title' => 'Slider Subtitle',
                'name' => 'subtitle',
                'type' => 'text',
                'value' => $slider->subtitle,
                'required' => 'false'
                ])
            @include('layouts.shared.fields.textarea', [
                'title' => 'Slider Description',
                'name' => 'description',
                'ckeditor' => 'true',
                'value' => $slider->description,
                'required' => 'false'
                ])
            @if(isset($slider->image_desktop))
                <div>
                    <img src="{{ asset("storage/$slider->image_desktop") }}" alt="" class="img-responsive"> <br />
                    <a onclick="return confirm('Are you sure?')" href="{{ route('admin.slider.remove.image_desktop', ['shop' => $slider->id, 'image' => substr($slider->image_desktop, 10)]) }}" class="btn btn-danger btn-sm">Remove image?</a><br />
                </div>
            @endif
            @include('layouts.shared.fields.input', [
                'title' => 'Image Desktop',
                'name' => 'image_desktop',
                'type' => 'file',
                'required' => 'false'
                ])
            @if(isset($slider->image_mobile))
                <div>
                    <img src="{{ asset("storage/$slider->image_mobile") }}" alt="" class="img-responsive"> <br />
                    <a onclick="return confirm('Are you sure?')" href="{{ route('admin.slider.remove.image_mobile', ['shop' => $slider->id, 'image' => substr($slider->image_mobile, 10)]) }}" class="btn btn-danger btn-sm">Remove image?</a><br />
                </div>
            @endif
            @include('layouts.shared.fields.input', [
                'title' => 'Image Mobile',
                'name' => 'image_mobile',
                'type' => 'file',
                'required' => 'false'
                ])
            @include('layouts.shared.fields.input', [
                'title' => 'URL Key',
                'name' => 'url',
                'type' => 'text',
                'value' => $slider->url,
                'required' => 'true'
                ])
            @include('admin.shared.status-select', [
                'status' => $slider->status
                ])
        </div>
        <div class="card-footer text-right">
            <div class="d-flex">
                <a href="{{ route('admin.slider.index') }}" class="btn btn-link">Cancel</a>
                <button type="submit" class="btn btn-primary ml-auto">Save Slider</button>
            </div>
        </div>
    </form>

@endsection

@section('footerjs')
    <script src="{{ asset('//cdn.ckeditor.com/4.8.0/standard/ckeditor.js') }}"></script>
@endsection
