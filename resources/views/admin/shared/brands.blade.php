<div class="form-group">
        <label for="brands" class="form-label">Brand</label>
        <select class="form-control custom-select" name="brands[]" id="brands">
            @foreach($brands as $brand)
                <option @if($brand->id == $id )selected="selected" @endif value="{{ $brand->id }}">{{ $brand->name }}</option>
            @endforeach
        </select>
</div>