@foreach($children as $child)
    {{ $parent_name }} @if($child_parent_name)/ {{ $child_parent_name }} @endif / {{ $child->name }} /
        @if(count($child->children))
            @if(isset($selected_child))
                @php(
                    $selectedCh = $selected_child
                )
            @else
                @php(
                    $selectedCh = ''
                )
            @endif
            @include('admin.shared.children-text',[
                    'children' => $child->children,
                    'child_parent_name' => $child->name,
                    'selected_child' => $selectedCh
                    ]
            )
        @endif
@endforeach
