@foreach($children as $child)
    <option value="{{ $child->id }}" @if(isset($selected_child) && ($child->id == $selected_child)) selected @endif>{{ $parent_name }} @if($child_parent_name)/ {{ $child_parent_name }} @endif / {{ $child->name }}</option>
        @if(count($child->childrenFilter))
            @if(isset($selected_child))
                @php(
                    $selectedCh = $selected_child
                )
            @else
                @php(
                    $selectedCh = ''
                )
            @endif
            @include('admin.shared.children',[
                    'children' => $child->childrenFilter,
                    'child_parent_name' => $child->name,
                    'selected_child' => $selectedCh
                    ]
            )
        @endif
@endforeach
