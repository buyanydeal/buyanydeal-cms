@extends('layouts.admin.app')

@section('content')

    @include('layouts.errors-and-messages')

    <div class="card">
        <div class="card-header">
            <h3 class="card-title">{{ __('dashboard/users.manage-users') }}</h3>
            <div class="card-options">
                <a href="{{ route('admin.user.create') }}" class="btn btn-outline-primary btn-sm float-right"><i class="fe fe-plus mr-2"></i>{{ __('dashboard/users.add-users') }}</a>
            </div>
        </div>
        <div class="card-body table-responsive">
            <table class="table card-table table-vcenter">
                <thead>
                <tr>
                    <th>{{ __('dashboard/users.id') }}</th>
                    <th>{{ __('dashboard/users.name') }}</th>
                    <th>{{ __('dashboard/users.email') }}</th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr>
                        <td>{{ $user->id }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td class="w-1">
                            <form action="{{ route('admin.shop.destroy', $user->id) }}" method="post">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="delete">
                                <div class="btn-group">
                                    <button onclick="return confirm('Are you sure?')" type="submit" class="btn icon" style="padding:0; background:transparent">
                                        <i class="fe fe-trash"></i>
                                    </button>
                                </div>
                            </form>
                        </td>
                        <td class="w-1">
                            <a class="icon" href="{{ route('admin.user.edit', $user->id) }}">
                                <i class="fe fe-edit"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection