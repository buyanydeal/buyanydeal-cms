@extends('layouts.admin.app')

@section('content')

    @include('layouts.errors-and-messages')
    <form action="{{ route('admin.product.update', $product->id) }}" method="post" enctype="multipart/form-data">
        <input type="hidden" name="_method" value="put">
        {{ csrf_field() }}
        <div class="card">
            <div class="card-header">
                <h5 class="mb-0">General</h5>
            </div>

            <div class="card-body">
                @include('layouts.shared.fields.input', [
                    'title' => 'Product Name',
                    'name' => 'name',
                    'type' => 'text',
                    'value' => $product->name,
                    'required' => 'true'
                    ])
                @include('layouts.shared.fields.textarea', [
                    'title' => 'Description',
                    'name' => 'description',
                    'ckeditor' => 'true',
                    'value' => $product->description,
                    'required' => 'false'
                    ])
                @include('layouts.shared.fields.input', [
                    'title' => 'Slug',
                    'name' => 'slug',
                    'type' => 'text',
                    'value' => $product->slug,
                    'required' => 'true'
                    ])
                <p><a href="{{route('front.product.slug',['slug' => $product->slug])}}" target="_blank">Show product</a></p>
                @include('admin.shared.brands', ['brands' => $brands, 'id' => $product->brand->id])
                @include('admin.shared.status-select', ['status' => $product->status])
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                <h5 class="mb-0">ERP</h5>
            </div>
            <div class="card-body">
                @include('layouts.shared.fields.input', [
                    'title' => 'Sku',
                    'name' => 'sku',
                    'type' => 'text',
                    'value' => $product->sku,
                    'required' => 'true'
                    ])
                @include('layouts.shared.fields.input', [
                    'title' => 'Ean',
                    'name' => 'ean',
                    'type' => 'text',
                    'value' => $product->ean,
                    'required' => 'false'
                    ])
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                <h5 class="mb-0">Images</h5>
            </div>
            <div class="card-body">
                @if(isset($product->cover))
                    <div class="form-group">
                        <img src="{{ asset("storage/$product->cover") }}" alt="" class="img-responsive img-thumbnail" width="150">
                    </div>
                @endif
                @include('layouts.shared.fields.input', [
                    'title' => 'Product Main Image',
                    'name' => 'cover',
                    'type' => 'text',
                    'value' => $product->cover,
                    'required' => 'false'
                    ])

                    <div class="form-group">
                        <div class="row">
                        @foreach($images as $image)
                            <div class="col-md-3">
                                <img src="{{ asset("storage/$image->src") }}" alt="" class="img-responsive img-thumbnail" width="200"> <br /> <br>
                                <a onclick="return confirm('Are you sure?')" href="{{ route('admin.product.remove.thumb', ['src' => $image->src]) }}" class="btn btn-danger btn-sm btn-block">Remove?</a><br />
                            </div>
                        @endforeach
                        </div>
                    </div>
                    <div class="row"></div>
                    <div class="form-group">
                        <label for="image">Images </label>
                        <input type="file" name="image[]" id="image" class="form-control" multiple>
                        <span class="text-warning">You can use ctr (cmd) to select multiple images</span>
                    </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                <h5 class="mb-0">Shops</h5>
            </div>
            <div class="card-body"><div class="table-responsive">
                    <table class="table mb-0">
                        <thead>
                        <tr>
                            <th class="pl-0">Shop</th>
                            <th>Original Price</th>
                            <th>Special Price</th>
                            <th>Lowest Price</th>
                            <th>Delivery Cost</th>
                            <th>Delivery Time</th>
                            <th>Shop URL</th>
                            <th class="pr-0">Status</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($product->productshop as $shop)
                                <tr>
                                    <td>
                                        @include('layouts.shared.fields.input', [
                                            'name' => 'shop[id]',
                                            'type' => 'text',
                                            'value' => $shop->id,
                                            'required' => 'true'
                                            ])
                                    </td>
                                    <td>
                                        @include('layouts.shared.fields.input', [
                                            'name' => 'shop[id]',
                                            'type' => 'text',
                                            'value' => $shop->original_price,
                                            'required' => 'true'
                                            ])
                                    </td>
                                    <td>
                                        @include('layouts.shared.fields.input', [
                                            'name' => 'shop[id]',
                                            'type' => 'text',
                                            'value' => $shop->special_price,
                                            'required' => 'false'
                                            ])
                                    </td>
                                    <td>
                                        @include('layouts.shared.fields.input', [
                                            'name' => 'shop[id]',
                                            'type' => 'text',
                                            'value' => $shop->lowest_price,
                                            'required' => 'true'
                                            ])
                                    </td>
                                    <td>
                                        @include('layouts.shared.fields.input', [
                                            'name' => 'shop[id]',
                                            'type' => 'text',
                                            'value' => $shop->delivery_cost,
                                            'required' => 'true'
                                            ])
                                    </td>
                                    <td>
                                        @include('layouts.shared.fields.input', [
                                            'name' => 'shop[id]',
                                            'type' => 'text',
                                            'value' => $shop->delivery_time,
                                            'required' => 'true'
                                            ])
                                    </td>
                                    <td>
                                        @include('layouts.shared.fields.input', [
                                            'name' => 'shop[id]',
                                            'type' => 'text',
                                            'value' => $shop->url,
                                            'required' => 'true'
                                            ])
                                    </td>
                                    <td class="pr-0">
                                        {{ $shop->stock_status  }}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                <h5 class="mb-0">Categories</h5>
            </div>
            <div class="card-body">
                <div id="tree">
                    @include('admin.product.includes.categories', ['categories' => $categories, 'ids' => $product])
                </div>
            </div>
        </div>

        {{--<div class="card">--}}
            {{--<div class="card-header">--}}
                {{--<h5 class="mb-0">Categories</h5>--}}
            {{--</div>--}}
            {{--<div class="card-body">--}}
                {{--<div id="tree">--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}

        <div class="card">
            <div class="card-header">
                <h5 class="mb-0">SEO Information</h5>
            </div>
            <div class="card-body">
                @include('admin.shared.meta-edit', [
                    'meta_title' => $product->meta_title,
                    'meta_description' => $product->meta_description,
                    'meta_keywords' => $product->meta_keywords])
            </div>
        </div>

        <div class="card">
            <div class="card-body text-right">
                <div class="d-flex">
                    <a href="{{ route('admin.product.index') }}" class="btn btn-link">Cancel</a>
                    <button type="submit" class="btn btn-primary ml-auto">Save Product</button>
                </div>
            </div>
        </div>
    </form>

@endsection

@section('footerjs')

    <script>
        // require(['jquery', 'jstree'], function ($) {
        //     $("#tree").jstree({
        //         "core": {
        //         },
        //         "checkbox": {
        //             "three_state": false,
        //             "visible": false
        //         },
        //         "plugins" : [ "themes", "checkbox" ]
        //     }).on("click", ".jstree-anchor", function(e) {
        //         let checkbox = $(this).find('input');
        //
        //         if (checkbox.prop( "checked")) {
        //             checkbox.attr( "checked", true );
        //         } else {
        //             checkbox.attr( "checked", false );
        //         }
        //
        //         // node.trigger("change", { field: "checked" });
        //     });
        // });


    </script>
    <script src="{{ asset('//cdn.ckeditor.com/4.8.0/standard/ckeditor.js') }}"></script>
@endsection
