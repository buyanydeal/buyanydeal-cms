
<ul class="list-unstyled" style="padding-left: 25px">
    @foreach($categories as $category)
        <li>
            <div class="checkbox">
                <label>
                    <input
                            type="checkbox"
                            @if(isset($selectedCatIds) && in_array($category->id, $selectedCatIds))checked="checked" @endif
                            name="categories[]"
                            value="{{ $category->id }}">
                    {{ $category->name }}
                </label>
            </div>
            @if($category->kids->count() >= 1)
                @include('admin.product.includes.category-children', ['categories' => $category->kids, 'selectedCatIds' => $selectedCatIds])
            @endif
        </li>
    @endforeach
</ul>