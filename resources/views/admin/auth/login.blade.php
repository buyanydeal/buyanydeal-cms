@extends('layouts.admin.blank')

@section('headcode')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('content')
    @include('layouts.errors-and-messages')
    <div class="text-center mb-6">
        @include('layouts.admin.includes.logo')
        <form class="card text-left" action="{{ route('admin.login') }}" method="post">
            {{ csrf_field() }}
            <div class="card-body p-6">
                <div class="card-title">Login to your account</div>
                <div class="form-group">
                    <label class="form-label">Email address</label>
                    <input type="email" name="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Enter email">
                </div>
                <div class="form-group">
                    <label class="form-label">
                        Password
                        <a href="./forgot-password.html" class="float-right small">I forgot password</a>
                    </label>
                    <input type="password" name="password" class="form-control" id="password" placeholder="Password">
                </div>
                <div class="form-group">
                    <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" />
                        <span class="custom-control-label">Remember me</span>
                    </label>
                </div>
                <div class="form-footer">
                    <button type="submit" class="btn btn-primary btn-block">Sign in</button>
                </div>
            </div>
        </form>
    </div>
@endsection