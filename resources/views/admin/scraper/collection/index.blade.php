@extends('layouts.admin.app')

@section('content')

    @include('layouts.errors-and-messages')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Manage Scraper Collections</h3>
            <div class="card-options">
                <a href="{{ route('admin.scrapercollection.create') }}" class="btn btn-outline-primary btn-sm float-right"><i class="fe fe-plus mr-2"></i>Add Scraper Collection</a>
            </div>
        </div>
        <div class="card-body table-responsive">
            <table class="table card-table table-vcenter">
                <thead>
                <tr>
                    <th>id</th>
                    <th>Name</th>
                    <th>Shop</th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                    @foreach($collections as $collection)
                        <tr>
                            <td>{{ $collection->id }}</td>
                            <td>{{ $collection->name }}</td>
                            <td>{{ $collection->shop->name }}</td>
                            <td class="w-1">
                                <form action="{{ route('admin.scrapercollection.destroy', $collection->id) }}" method="post">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="delete">
                                    <div class="btn-group">
                                        <button onclick="return confirm('Are you sure?')" type="submit" class="btn icon" style="padding:0; background:transparent">
                                            <i class="fe fe-trash"></i>
                                        </button>
                                    </div>
                                </form>
                            </td>
                            <td class="w-1">
                                <a class="icon" href="{{ route('admin.scrapercollection.edit', $collection->id) }}">
                                    <i class="fe fe-edit"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection