<ul>
    @foreach($categories as $category)
        <li>
            <label>
                <input
                        type="checkbox"
                        name="categories[]"
                        value="{{ $category->id }}" class="inputcheck">
                {{ $category->name }}
            </label>
            @if($category->kids->count() >= 1)
                @include('admin.scraper.collection.includes.category-children', ['categories' => $category->kids])
            @endif
        </li>
    @endforeach
</ul>