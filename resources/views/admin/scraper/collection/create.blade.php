@extends('layouts.admin.app')

@section('content')

    @include('layouts.errors-and-messages')

    <form action="{{ route('admin.scrapercollection.store') }}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}

        <div class="card">
            <div class="card-body">
                <h3 class="card-title">Add new Collection</h3>
                @include('layouts.shared.fields.input', [
                    'title' => 'Name',
                    'name' => 'name',
                    'type' => 'text',
                    'required' => 'true'
                    ])
                @include('layouts.shared.fields.input', [
                    'title' => 'URL',
                    'name' => 'collection',
                    'type' => 'text',
                    'required' => 'true'
                    ])

                <div class="form-group">
                    <label for="shop_id" class="form-label">Shop</label>
                    <select id="shop_id" name="shop_id" class="form-control custom-select">
                        @foreach($shops as $shop)
                            <option value="{{ $shop->id }}">{{ $shop->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                <h5 class="mb-0">Categories</h5>
            </div>
            <div class="card-body">
                <div id="tree">
                    @include('admin.scraper.collection.includes.categories', ['categories' => $categories])
                </div>
            </div>
        </div>
        <div class="card-footer text-right">
            <div class="d-flex">
                <a href="{{ route('admin.scrapercollection.index') }}" class="btn btn-link">Cancel</a>
                <button type="submit" class="btn btn-primary ml-auto">Add ScraperCollection</button>
            </div>
        </div>
    </form>

@endsection

@section('footerjs')
    <script src="{{ asset('//cdn.ckeditor.com/4.8.0/standard/ckeditor.js') }}"></script>
@endsection