@extends('layouts.admin.app')

@section('content')

    @include('layouts.errors-and-messages')

    <div class="card">
        <div class="card-header">
            <h3 class="card-title">{{ __('admin.manage-categories') }}</h3>
            <div class="card-options">
                <form action="{{ route('admin.category.index') }}" class="mr-2">
                    <input type="text" name="q" class="form-control" placeholder="Search..." value="{!! request()->input('q') !!}" />
                </form>
                <a href="{{ route('admin.category.create') }}" class="btn btn-outline-primary float-right"><i class="fe fe-plus mr-2"></i>{{ __('admin.add-categories') }}</a>
            </div>
        </div>
        <div class="card-body table-responsive">
            @if($categories)
                <table class="table card-table table-vcenter">
                    <thead>
                    <tr>
                        <th></th>
                        <th>{{ __('admin.id') }}</th>
                        <th>{{ __('admin.name') }}</th>
                        <th>{{ __('admin.slug') }}</th>
                        <th>{{ __('admin.active') }}</th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($categories as $category)
                        <tr>
                            <td><img src="{{ asset("storage/$category->cover") }}" alt="" class="h-8"></td>
                            <td>{{ $category->id }}</td>
                            <td>{{ $category->getParentsNamesFilter() }}</td>
                            <td><a href="/{{ $category->slug }}.html" target="_blank">{{ $category->slug }}</a></td>
                            <td>
                                @if($category->status == 1)
                                    <span class="badge badge-success">Enabled</span>
                                @else
                                    <span class="badge badge-danger">Disabled</span>
                                @endif
                            </td>
                            <td class="w-1">
                                <form action="{{ route('admin.category.destroy', $category->id) }}" method="post">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="delete">
                                    <div class="btn-group">
                                        <button onclick="return confirm('Are you sure?')" type="submit" class="btn icon" style="padding:0; background:transparent">
                                            <i class="fe fe-trash"></i>
                                        </button>
                                    </div>
                                </form>
                            </td>
                            <td class="w-1">
                                <a class="icon" href="{{ route('admin.category.edit', $category->id) }}">
                                    <i class="fe fe-edit"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>

    <div class="toolbar d-flex justify-content-end">
        <nav class="justify-content-end" aria-label="category navigation">
            @if($categories instanceof \Illuminate\Contracts\Pagination\LengthAwarePaginator)
                {{ $categories->links() }}
            @endif
        </nav>
    </div>

@endsection
