@extends('layouts.admin.app')

@section('content')

    @include('layouts.errors-and-messages')

    <form action="{{ route('admin.category.update', $category->id) }}" method="post" class="card" enctype="multipart/form-data">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="put">
        <div class="card-body">
            <h3 class="card-title">Edit Category - {{ $category->name }}</h3>
            <div class="form-group">
                <label for="name" class="form-label">Category Name</label>
                <input id="name" name="name" type="text" class="form-control" value="{{ $category->name ?: old('name') }}">
            </div>
            <div class="form-group">
                <label for="page_title" class="form-label">Page Title</label>
                <input id="page_title" name="page_title" type="text" class="form-control" value="{{ $category->page_title ?: old('page_title') }}">
            </div>
            <div class="form-group">
                <label for="slug" class="form-label">Slug</label>
                <input id="slug" name="slug" type="text" class="form-control" value="{{ $category->slug ?: old('slug') }}">
            </div>
            @include('admin.shared.status-select', ['status' => $category->status])
            <div class="form-group">
                <label for="description" class="form-label">Content</label>
                <textarea class="form-control ckeditor" name="description" id="description" rows="5" placeholder="Content">{{ $category->description ?: old('description') }}</textarea>
            </div>
            <div class="form-group">
                <label for="seo_description" class="form-label">Bottom Text</label>
                <textarea class="form-control ckeditor" name="seo_description" id="seo_description" rows="5" placeholder="Content">{{ $category->seo_description ?: old('seo_description') }}</textarea>
            </div>
            @if(isset($category->cover))
                <div>
                    <img src="{{ asset("storage/$category->cover") }}" alt="" class="img-responsive"> <br />
                    <a onclick="return confirm('Are you sure?')" href="{{ route('admin.category.remove.image', ['category' => $category->id, 'image' => substr($category->cover, 10)]) }}" class="btn btn-danger btn-sm">Remove image?</a><br />
                </div>
            @endif
            <div class="form-group">
                <label for="cover">Image</label>
                <input type="file" name="cover" id="cover" class="form-control">
            </div>
            @if(isset($category->thumbnail))
                <div>
                    <img src="{{ asset("storage/$category->thumbnail") }}" alt="" class="img-responsive"> <br />
                    <a onclick="return confirm('Are you sure?')" href="{{ route('admin.category.remove.image', ['category' => $category->id, 'image' => substr($category->thumbnail, 10)]) }}" class="btn btn-danger btn-sm">Remove image?</a><br />
                </div>
            @endif
            <div class="form-group">
                <label for="thumbnail" class="form-label">Thumbnail</label>
                <input id="thumbnail" name="thumbnail" type="file" class="form-control" value="">
            </div>
            @if(isset($category->icon))
                <div>
                    <img src="{{ asset("storage/$category->icon") }}" alt="" class="img-responsive"> <br />
                    <a onclick="return confirm('Are you sure?')" href="{{ route('admin.category.remove.image', ['category' => $category->id, 'image' => substr($category->icon, 10)]) }}" class="btn btn-danger btn-sm">Remove image?</a><br />
                </div>
            @endif
            <div class="form-group">
                <label for="icon" class="form-label">Icon</label>
                <input id="icon" name="icon" type="file" class="form-control" value="">
            </div>
            @include('admin.shared.meta-edit', ['meta_title' => $category->meta_title, 'meta_description' => $category->meta_description, 'meta_keywords' => $category->meta_keywords])

            <div class="form-group">
                <label for="parent_id" class="form-label">{{ __('admin.parent-category') }}</label>
                <select id="parent_id" name="parent_id" class="form-control custom-select">
                    <option value="">{{ __('admin.no-parent') }}</option>
                    @foreach($categories as $cat)
                        <option value="{{ $cat->id }}" @if($cat->id == $category->parent_id) selected @endif>{{ $cat->name }}</option>
                        @if(count($cat->childrenFilter))
                            @include('admin.shared.children',[
                                'children' => $cat->childrenFilter,
                                'parent_name' => $cat->name,
                                'child_parent_name' => '',
                                'selected_child' => $category->parent_id
                                ]
                            )
                        @endif

                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="sort_order" class="form-label">Sorting</label>
                <input id="sort_order" name="sort_order" type="text" class="form-control" value="{{ $category->sort_order ?: old('sort_order') }}">
            </div>
        </div>
        <div class="card-footer text-right">
            <div class="d-flex">
                <a href="{{ route('admin.category.index') }}" class="btn btn-link">Cancel</a>
                <button type="submit" class="btn btn-primary ml-auto">Save Category</button>
            </div>
        </div>
    </form>

@endsection

@section('footerjs')
    <script src="{{ asset('//cdn.ckeditor.com/4.8.0/standard/ckeditor.js') }}"></script>
@endsection
