<!doctype html>
<html lang="en">
<head>
    @include('layouts.admin.includes.head')
    @yield('headcode')
</head>
<body class="">
<div class="page">
    <div class="page-single">
        <div class="container">
            <div class="row">
                <div class="col col-login mx-auto">
                    @yield('content')
                </div>
            </div>
        </div>
    </div>
</div>
@yield('footerjs')
</body>
</html>
