<ul class="nav nav-tabs border-0 flex-column flex-lg-row">
    <li class="nav-item">
        <a href="{{ route('admin.dashboard') }}" class="nav-link"><i class="fe fe-home"></i> {{ __('dashboard/menu.dashboard') }}</a>
    </li>
    <li class="nav-item">
        <a href="javascript:void(0)" class="nav-link" data-toggle="dropdown">
            <i class="fe fe-shopping-bag"></i> {{ __('dashboard/menu.catalog') }}
        </a>
        <div class="dropdown-menu dropdown-menu-arrow">
            <a href="{{ route('admin.product.index') }}" class="dropdown-item ">{{ __('dashboard/menu.products') }}</a>
            <a href="{{ route('admin.category.index') }}" class="dropdown-item ">{{ __('dashboard/menu.categories') }}</a>
            <div class="dropdown-divider"></div>
            <a href="{{ route('admin.shop.index') }}" class="dropdown-item ">{{ __('dashboard/menu.shops') }}</a>
            <div class="dropdown-divider"></div>
            <a href="{{ route('admin.brand.index') }}" class="dropdown-item ">{{ __('dashboard/menu.brands') }}</a>
        </div>
    </li>
    <li class="nav-item">
        <a href="javascript:void(0)" class="nav-link" data-toggle="dropdown">
            <i class="fe fe-shopping-bag"></i> {{ __('dashboard/menu.scraper') }}
        </a>
        <div class="dropdown-menu dropdown-menu-arrow">
            <a href="{{ route('admin.scrapercollection.index') }}" class="dropdown-item ">{{ __('dashboard/menu.collections') }}</a>
        </div>
    </li>
    <li class="nav-item">
        <a href="javascript:void(0)" class="nav-link" data-toggle="dropdown">
            <i class="fe fe-shopping-bag"></i> {{ __('dashboard/menu.cms') }}
        </a>
        <div class="dropdown-menu dropdown-menu-arrow">
            <a href="{{ route('admin.slider.index') }}" class="dropdown-item ">{{ __('dashboard/menu.sliders') }}</a>
        </div>
    </li>
    <li class="nav-item">
        <a href="javascript:void(0)" class="nav-link" data-toggle="dropdown">
            <i class="fe fe-shopping-bag"></i> {{ __('dashboard/menu.system') }}
        </a>
        <div class="dropdown-menu dropdown-menu-arrow">
            <a href="{{ route('admin.user.index') }}" class="dropdown-item ">{{ __('dashboard/menu.users') }}</a>
        </div>
    </li>
</ul>