<div class="desktop-nav bg-white d-none d-md-block">
    <div class="container py-2">
        <div class="main-nav-wrapper d-flex">
            <div class="category-nav-wrapper">
                <button class="btn-department" id="megaMenuToggle">
                    <i class="fas fa-bars" id="nav_toggle"></i> <span class="ml-2">Browse by category</span> <i class="fas fa-chevron-down"></i>
                </button>
                <div class="nav-dropdown-wrapper">
                    <ul class="level-1 expand" id="categoryNav">
                        @foreach($categories as $category)
                                <li class="list-item @if ($loop->first) active @endif">
                                    <a
                                            href="{{ route('front.category.view', str_slug($category->slug)) }}"
                                            title="{{ $category->name }}"
                                            data-event-on="click::ga('send', 'event', 'desktop_content', 'desktop_navigation', '/{{ $category->slug }}.html', 0, {'nonInteraction': 1});"
                                    >{{ $category->name }} <i class="fas fa-chevron-right"></i></a>
                                    <div class="level-2-container">
                                        <div class="content-wrapper">
                                            <ul class="level-2">
                                                @foreach ($category->children as $level)
                                                    @if ($level->slug !== NULL && $level->status !== 0)
                                                        <li>
                                                            <a
                                                                    href="{{ route('front.category.view', str_slug($level->slug)) }}"
                                                                    title="{{ $level->name }}"
                                                                    data-event-on="click::ga('send', 'event', 'desktop_content', 'desktop_navigation', '/{{ $level->slug }}.html', 0, {'nonInteraction': 1});"
                                                            >
                                                                {{ $level->name }}
                                                            </a>
                                                            <ul class="level-3">
                                                                @foreach ($level->children as $child)
{{--                                                                    @if($child->products->count())--}}
                                                                        @if ($child->slug !== NULL && $child->status !== 0)
                                                                            <li>
                                                                                <a
                                                                                        href="{{ route('front.category.view', str_slug($child->slug)) }}"
                                                                                        title="{{ $child->name }}"
                                                                                        data-event-on="click::ga('send', 'event', 'desktop_content', 'desktop_navigation', '/{{ $child->slug }}.html', 0, {'nonInteraction': 1});"
                                                                                >{{ $child->name }}</a>
                                                                            </li>
                                                                        @endif
                                                                    {{--@endif--}}
                                                                @endforeach
                                                            </ul>
                                                        </li>
                                                    @endif
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <ul class="nav-landing-pages p-0 m-0">
                <li class="nav-landing-item">
                    <a href="#" title="Compare Brand Prices">Apple</a>
                </li>
                <li class="nav-landing-item">
                    <a href="#" title="Compare Brand Prices">Pampers</a>
                </li>
                <li class="nav-landing-item">
                    <a href="#" title="Compare Brand Prices">Coffee Machines</a>
                </li>
                <li class="nav-landing-item">
                    <a href="{{ route('front.brand.index')}}" title="Compare Brand Prices">Coupon Codes</a>
                </li>
                <li class="nav-landing-item">
                    <a href="{{ route('front.brand.index')}}" title="Compare Brand Prices">Brands</a>
                </li>
                <li class="nav-landing-item">
                    <a href="{{ route('front.shop.index')}}" title="Compare Stores">Stores</a>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="megamenu-overlay"></div>