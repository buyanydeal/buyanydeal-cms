<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

{{-- SEO Data --}}
@if(isset($pageTitle))
    <title>{{ $pageTitle }}</title>
@else
    <title>SEO Title</title>
@endif

@if(isset($metaDescription))
    <meta name="description" content="{{ $metaDescription }}" />
@endif
@if(isset($metaKeywords))
    <meta name="keywords" content="{{ $metaKeywords }}" />
@endif

@if(isset($canonical))
    <link rel="canonical" href="{{ $canonical }}"/>
    <link rel="alternate" href="{{ $canonical }}" hreflang="en-ae">
@endif
<meta name="referrer" content="always">
<meta name="robots" content="index, follow">

{{-- Google Tags --}}
<link id="googleLink" rel="publisher" href="https://plus.google.com/u/1/108306355169323170549" />
<meta name="google-site-verification" content="b7FOWJZj09XGYfM_QlHi6yUpOsnCB39fpviXQ48r6EU" />

{{-- Facebook Tags --}}
<meta property="og:type" content="website">
@if(isset($image))
    <meta property="og:image" content="{{ $image }}">
@else
    <meta property="og:image" content="https://buyanydeal.com/images/social/facebook.jpg">
@endif
<meta property="og:title" content="{{ $pageTitle }}">
<meta property="og:description" content="{{ $metaDescription }}">
<meta property="og:site_name" content="Buyanydeal UAE">
<meta property="og:url" content="{{ $canonical }}">

{{-- Twitter --}}
<meta name="twitter:card" content="summary">
<meta name="twitter:site" content="@buyanydeal">
@if(isset($image))
    <meta property="twitter:image" content="{{ $image }}">
@else
    <meta name="twitter:image" content="https://buyanydeal.com/images/social/twitter.jpg">
@endif
<meta name="twitter:title" content="{{ $pageTitle }}">
<meta name="twitter:description" content="{{ $metaDescription }} https://buyanydeal.com/en">

{{-- Icons --}}
<link rel="icon" sizes="16x16" href="{{ url('images/favicon/favicon-16.png') }}" />

<link rel="apple-touch-icon" sizes="57x57" href="{{ url('images/favicon/apple-icon-57x57.png') }}">
<link rel="apple-touch-icon" sizes="60x60" href="{{ url('images/favicon/apple-icon-60x60.png') }}">
<link rel="apple-touch-icon" sizes="72x72" href="{{ url('images/favicon/apple-icon-72x72.png') }}">
<link rel="apple-touch-icon" sizes="76x76" href="{{ url('images/favicon/apple-icon-76x76.png') }}">
<link rel="apple-touch-icon" sizes="114x114" href="{{ url('images/favicon/apple-icon-114x114.png') }}">
<link rel="apple-touch-icon" sizes="120x120" href="{{ url('images/favicon/apple-icon-120x120.png') }}">
<link rel="apple-touch-icon" sizes="144x144" href="{{ url('images/favicon/apple-icon-144x144.png') }}">
<link rel="apple-touch-icon" sizes="152x152" href="{{ url('images/favicon/apple-icon-152x152.png') }}">
<link rel="apple-touch-icon" sizes="180x180" href="{{ url('images/favicon/apple-icon-180x180.png') }}">
<link rel="icon" type="image/png" sizes="192x192"  href="{{ url('images/favicon/android-icon-192x192.png') }}">
<link rel="icon" type="image/png" sizes="32x32" href="{{ url('images/favicon/favicon-32x32.png') }}">
<link rel="icon" type="image/png" sizes="96x96" href="{{ url('images/favicon/favicon-96x96.png') }}">
<link rel="icon" type="image/png" sizes="16x16" href="{{ url('images/favicon/favicon-16x16.png') }}">
<link rel="manifest" href="{{ url('manifest/manifest.json') }}">

<meta name="msapplication-TileColor" content="#33b57b">
<meta name="msapplication-TileImage" content="{{ url('images/favicon/ms-icon-144x144.png') }}">
<meta name="msapplication-config" content="{{ url('images/favicon/browserconfig.xml') }}" />
<meta name="HandheldFriendly" content="True" />
<meta name="MobileOptimized" content="320" />
<meta name="theme-color" content="#33b57b">

<meta name="verify-admitad" content="b7e170e473" />

<link href="{{ mix('css/app.css') }}" rel="stylesheet" />
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous" />

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TM9DMS3');</script>
<!-- End Google Tag Manager -->
