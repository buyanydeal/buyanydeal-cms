<header class="mobile-header bg-white d-md-none">
    <div class="mobile-header-top py-2 bg-primary text-center position-fixed">
        <i class="fas fa-bars text-white" id="nav_toggle"></i>
        <a href="{{ route('front.home.index') }}" class="header-logo text-white" title="Your online shopping mall in the UAE">
            <img src="{{ url('images/logo.png') }}" alt="buyanydeal.com"/>
        </a>
        <a href="#" class="text-white" title="">
            <i class="far fa-user text-white"></i>
    </div>
    <div class="mobile-header-search">
        <form class="my-0 position-relative mobile-search">
            <input class="form-control mr-sm-2" type="search" placeholder="Try Iphone" aria-label="Search">
            <button class="btn btn position-absolute" type="submit"><i class="fa fa-search"></i></button>
        </form>
    </div>
</header>

{{--@include('layouts.front.includes.main-nav-mobile')--}}

@include('layouts.front.includes.main-nav-desktop')
