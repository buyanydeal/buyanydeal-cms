<div class="sidebar-nav">
    <div class="overlay"></div>
    <div class="sidebar-nav-wrapper">
        <div class="sidebar-nav-profile">
            <a href="/" class="profile-pic" title="Online Shopping"></a>
            <div class="profile-user">
                <h4 class="profile-user-name">
                    <span>Hi there</span>, <span class="profile-user-logout font-weight-normal">select a category below.</span>
                </h4>

            </div>
            <a href="#" class="profile-sidebar-close" title="open navbar"></a>
        </div>
        <ul class="sidebar-nav-list">
            @foreach($categories as $category)
                <li class="sidebar-nav-item nested-list-item">
                    <a href="{{ route('front.category.view', str_slug($category->slug)) }}" title="{{ $category->name }}" class="toggle-link sidebar-nav-item-link list-women nav-anchor">{{ $category->name }}</a>
                    <div class="toggle-content">
                        <ul class="nested-list">
                            @foreach ($category->children as $level)
                                <li class="nested-list-item">
                                    <a class="nav-anchor nested-list-item-link" href="{{ route('front.category.view', str_slug($level->slug)) }}" title="{{ $level->name }}">
                                        {{ $level->name }}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </li>
            @endforeach
            <li class="sidebar-nav-item nested-list-item">
                <a class="toggle-link sidebar-nav-item-link list-women nav-anchor" href="{{ route('front.brand.index')}}" title="Brands">Brands</a>
            </li>
        </ul>
    </div>
</div>