<footer class="page-footer bg-light">
  <div class="container">
    <div class="row">
      <div class="col-md-6 d-none d-lg-block">
        <strong class="block-title clearfix">Site map</strong>
        <div class="leaf">
          <ul>
            <li><a href="{{route('front.category.view',['slug' => 'women-fashion'])}}" title="women store">Women</a>
            <li><a href="{{route('front.category.view',['slug' => 'men-fashion'])}}" title="Men store">Men</a>
            <li><a href="{{route('front.category.view',['slug' => 'baby-kids'])}}" title="Baby and kids store">Babies & Kids</a>
          </ul>
        </div>
        <div class="leaf">
          <ul>
            <li><a href="{{route('front.category.view',['slug' => 'tv-audio'])}}" title="TV & Audio">TV & Audio</a>
            <li><a href="{{route('front.category.view',['slug' => 'cameras-all-cameras'])}}" title="Camera">Cameras</a>
            <li><a href="{{route('front.category.view',['slug' => 'books'])}}" title="Books">Books</a>
          </ul>
        </div>
      </div>
      <div class="col-sm-12 col-md-6 col-lg-3  d-none d-md-block">
        <strong class="block-title">Buy Any Deal</strong>
        <ul>
        <li><a href="#" title="About Buy any Deal">About Us</a>
        <li><a href="#" title="Desclaimer">Desclaimer</a>
        <li><a href="mailto:info@buyanydeal.com" title="Advertise with us">Advertise with Us</a>
        <li><a href="mailto:info@buyanydeal.com" title="Contact buy any deal">Contact Us</a>
        </ul>
      </div>
      <div class="col-sm-12 col-md-6 col-lg-3">
        <div class="m-b-2 newsletter">
          <strong class="block-title">Newsletter</strong>
          <form class="form-inline my-1 my-lg-0 position-relative">
            <input type="email" class="form-control" id="newsletter" placeholder="Enter email">
            <button class="btn position-absolute" type="submit"><i class="fas fa-chevron-right"></i></button>
          </form>
        </div>
        <div class="social">
          <strong class="block-title">follow Us</strong>
          <ul class="d-flex">
            <li><a href="https://www.facebook.com/buyanydeal" title="Buy Any Deal Facebook" class="facebook" target="_blank" rel="nofollow"><i class="fab fa-facebook-square"></i></a></li>
            <li><a href="https://twitter.com/buyanydeal" title="Buy Any Deal twitter" class="facebook" target="_blank" rel="nofollow"><i class="fab fa-twitter-square"></i></a></li>
            {{--<li><a href="#" title="Buy Any Deal insta" class="facebook" target="_blank"><i class="fab fa-instagram"></i></a></li>--}}
          </ul>
        </div>
      </div>
    </div>
    <div class="footer-bottom">
      <div class="row">
        <div class="col-sm-12 col-md-7 d-none d-md-block">
          {{--<ul>--}}
            {{--<li><a href="#" title="Terms & Conditions">Terms & Conditions</a>--}}
            {{--<li><a href="#" title="Privacy Policy">Privacy Policy</a>--}}
            {{--<li><a href="#" title="Desclaimer">Desclaimer</a>--}}
          {{--</ul>--}}
        </div>
        <div class="col-sm-12 col-md-5 copy-right">
          <p>® Buyanydeal, All rights reserved</p>
        </div>
      </div> <!-- row-->
    </div> <!-- footer bottom-->
  </div> <!-- row -->
</footer>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="{{ mix('js/front/app.js') }}"></script>

{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>--}}
{{--<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>--}}
{{--<script src="{{ url('js/mediaCheck.min.js') }}"></script>--}}
<script async src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5b51a2681e2909ed"></script>
