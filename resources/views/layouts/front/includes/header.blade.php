<header class="header page__header">
    <div class="container header__container">
        <span class="header__menuicon">
            <i class="header__menuicon--icon fas fa-bars text-white" id="mobile__nav--toggle"><span class="text-hide">{{ __('header.menu') }}</span></i>
        </span>
        <a href="{{ route('front.home.index') }}" class="header__logo" title="{{ __('header.page-title') }}" aria-label="Buyanydeal">
            <img class="header__logo--img" src="{{ url('images/logo.png') }}" alt="{{ __('header.logo-alt') }}">
        </a>
        <form class="header__search" action="{{ route('search.product')}}" method="get">
            <div class="search__input">
                <i class="fa fa-arrow-left"  id="search_close"><span class="text-hide">{{ __('header.search') }}</span></i>
                <input name="q" class="header__search--input form-control" type="search" placeholder="{{ __('header.search-placeholder') }}" aria-label="Search" value="{!! request()->input('q') !!}">
                <button class="header__search--btn position-absolute" type="submit"><i class="fa fa-search"><span class="text-hide">{{ __('header.search') }}</span></i></button>
            </div>
        </form>
        <span class="header__searchicon">
            <i class="header__searchicon--icon fa fa-search text-white" id="search_toggle"><span class="text-hide">{{ __('header.search') }}</span></i>
        </span>
    </div>
</header>

<nav class="nav page__navigation">
    <div class="container nav__container">
        <ul class="mainnav">
            <li class="department mainnav__item">
                <button class="btn__department mainnav__item--link" id="megaMenuToggle">
                    <i class="btn__department--icon fas fa-bars" id="nav__toggle"><span class="text-hide">{{ __('header.menu') }}</span></i> <span class="btn__department--text ml-2">{{ __('header.browse-category') }}</span> <i class="btn__department--icon fas fa-chevron-down"><span class="text-hide">{{ __('header.menu') }}</span></i>
                </button>
            </li>
            {{--<li class="mainnav__item">--}}
                {{--<a class="mainnav__item--link" href="#" title="Compare Brand Prices">Apple</a>--}}
            {{--</li>--}}
            {{--<li class="mainnav__item">--}}
                {{--<a class="mainnav__item--link" href="#" title="Compare Brand Prices">Pampers</a>--}}
            {{--</li>--}}
            {{--<li class="mainnav__item">--}}
                {{--<a class="mainnav__item--link" href="#" title="Compare Brand Prices">Coffee Machines</a>--}}
            {{--</li>--}}
            <li class="mainnav__item">
                <a class="mainnav__item--link" href="{{ route('front.brand.index')}}" title="{{ __('header.compare-brand') }}">{{ __('header.brands') }}</a>
            </li>
            <li class="mainnav__item">
                <a class="mainnav__item--link" href="{{ route('front.shop.index')}}" title="{{ __('header.compare-stores') }}">{{ __('header.stores') }}</a>
            </li>
            {{--<li class="coupon mainnav__item">--}}
                {{--<a class="mainnav__item--link" href="">Coupon Codes</a>--}}
            {{--</li>--}}
        </ul>
    </div>
    <div class="catnav">
        <div class="catnav__wrapper">
            <div class="catnav__title">{{ __('header.select-category') }} <i class="btn__close--icon fas fa-times" id="catnav_close"><span class="text-hide">{{ __('header.close') }}</span></i></div>
            <ul class="catnav__menu catmenu" id="categoryNav">
                @foreach($categories as $category)
                    <li class="catmenu__item">
                        <a
                                class="catmenu__item--link"
                                href="{{ route('front.category.view', str_slug($category->slug)) }}"
                                title="{{ $category->name }}"
                                data-event-on="click::ga('send', 'event', 'desktop_content', 'desktop_navigation', '/{{ $category->slug }}.html', 0, {'nonInteraction': 1});"
                        >{{ $category->name }} <i class="fas fa-chevron-right"></i></a>
                        <div class="toggle-content">
                            <ul class="catnav__menu catmenu level-2">
                                @foreach ($category->children as $level)
                                    @if ($level->slug !== NULL && $level->status !== 0)
                                        <li class="catmenu__item">
                                            <a
                                                    class="catmenu__item--link"
                                                    href="{{ route('front.category.view', str_slug($level->slug)) }}"
                                                    title="{{ $level->name }}"
                                                    data-event-on="click::ga('send', 'event', 'desktop_content', 'desktop_navigation', '/{{ $level->slug }}.html', 0, {'nonInteraction': 1});"
                                            >
                                                {{ $level->name }}
                                            </a>
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
</nav>

