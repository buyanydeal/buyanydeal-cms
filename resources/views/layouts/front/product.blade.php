<!doctype html>
<html dir="ltr" lang="en">
<head>
    @include('layouts.front.includes.head')
</head>
<body class="pdp">
    <!-- Google Tag Manager (noscript) -->
    <noscript>
        <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TM9DMS3"
            height="0" width="0" style="display:none;visibility:hidden">
        </iframe>
    </noscript>
    <!-- End Google Tag Manager (noscript) -->
    <a id="skippy" class="sr-only sr-only-focusable" href="#content">
        <div class="container">
            <span class="skiplink-text">Skip to main content</span>
        </div>
    </a>
    @include('layouts.front.includes.header')

    <div class="pdp-header">
        <div class="container" id="content">
            @yield('content_top')
        </div>
    </div>

    <div class="container">
        <div class="main-content my-4">
            @yield('content')
        </div>
    </div>
    @include('layouts.front.includes.footer')
    @yield('footerjs')
</body>
</html>
