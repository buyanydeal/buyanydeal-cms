<!doctype html>
<html dir="ltr" lang="en">
    <head>
    @include('layouts.front.includes.head')
    @yield('head-script')
    </head>
    <body class="">
        <!-- Google Tag Manager (noscript) -->
        <noscript>
            <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TM9DMS3"
                height="0" width="0" style="display:none;visibility:hidden">
            </iframe>
        </noscript>
        <!-- End Google Tag Manager (noscript) -->
        @include('layouts.front.includes.header')
        <div class="container">
            @yield('before-content')
            <div class="main-content my-4">
                <div class="row">
                    <div class="col-md-3">
                        @yield('sidebar')
                    </div>
                    <div class="col-md-9">
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.front.includes.footer')
        @yield('footerjs')
    </body>
</html>
