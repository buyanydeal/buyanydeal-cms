@if($brand->cover && $brand->description)
    <div class="category-header d-none d-md-block d-flex">
        <div class="row align-items-center">
            <div class="col-md-6">
                <img class="img-fluid" src="{{ asset("storage/$brand->cover") }}" alt="{{ $brand->name }} Product Comparison" title="{{ $brand->name }}">
            </div>
            <div class="col-md-6 text-center">
                {!! $brand->description !!}
            </div>
        </div>
    </div>
@endif