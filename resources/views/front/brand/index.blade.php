@extends('layouts.front.app-sidebar',[
    'pageTitle' => 'Compare brands - #1 online shopping mall in UAE | buyanydeal.com',
    'metaDescription' => 'Price comparison for these brands with all online stores in the UAE. Buyanydeal is your Dubai Mall Online. √ Easy to search √ easy to filter',
    'metaKeywords' => 'online shopping, comparison, uae, dubai, Abu Dhabi, Al Ain, Sharjah, buy online, cheapest price',
    'canonical' => route('front.brand.index')
])

@section('head-script')
    {{ Breadcrumbs::view('breadcrumbs::json-ld', 'front.brand.index') }}
@endsection

@section('before-content')
    <div class="content-header">
        <div class="row">
            <div class="col-md-3">
                <div class="back-link">
                    <a href="{{ route('front.home.index')}}">
                        <i class="fa fa-chevron-left"></i> {{ __('brands.backlink') }}
                    </a>
                </div>
            </div>
            <div class="col-md-9">
                {{ Breadcrumbs::render('front.brand.index') }}
            </div>
        </div>
    </div>
@endsection

@section('sidebar')
@endsection

@section('content')
    <h1 class="h2 page-title">Compare Brands in the United Arab Emirates</h1>

    <ul class="brand-index">
        @foreach($sortedBrands as $letter => $brands)
            <li><a href="#{{ $letter }}">{{ $letter }}</a></li>
        @endforeach
    </ul>

    @foreach($sortedBrands as $letter => $brands)
        <h2 id="{{ $letter }}" class="h5">{{ $letter }}</h2>
        <ul class="brand-list">
            @foreach($brands as $brand)
                <li>
                    <a href="{{ route('front.get.brand', str_slug($brand->slug)) }}" title="{{ $brand->name }}">
                        {{ $brand->name }}
                    </a>
                </li>
            @endforeach
        </ul>
    @endforeach
@endsection