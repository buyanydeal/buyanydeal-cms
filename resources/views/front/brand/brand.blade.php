@extends('layouts.front.app-sidebar',[
    'pageTitle' => $brand->meta_title,
    'metaDescription' => $brand->meta_description,
    'metaKeywords' => $brand->meta_keywords,
    'canonical' => route('front.get.brand', str_slug($brand->slug))
])

@section('before-content')
    <div class="content-header">
        <div class="row">
            <div class="col-md-3">
                <div class="back-link">
                    <a href="{{ route('front.brand.index')}}">
                        <i class="fa fa-chevron-left"></i> {{ __('brands.brands-link') }}
                    </a>
                </div>
            </div>
            <div class="col-md-9">
                {{ Breadcrumbs::render('front.get.brand', $brand) }}
            </div>
        </div>
    </div>
@endsection

@section('sidebar')

@endsection

@section('content')
    <h1 class="h2">Compare {{ $brand->name }} products</h1>
    @include('front.brand.includes.header')

    <div class="category-nav-top">
        <div class="toolbar d-flex justify-content-between">
            <div class="product-count my-2">{{ $products->total() }} @if($products->total() == 1)Item @else Items @endif</div>
            <nav class="justify-content-end" aria-label="category navigation">
                  @include('layouts.shared.pagination', ['paginator' => $products])
            </nav>
        </div>
    </div>
    @include('front.brand.includes.product-list', ['products' => $products])

    <div class="toolbar d-flex justify-content-end">
        <nav class="justify-content-end" aria-label="category navigation">
            @include('layouts.shared.pagination', ['paginator' => $products])
        </nav>
    </div>
@endsection
