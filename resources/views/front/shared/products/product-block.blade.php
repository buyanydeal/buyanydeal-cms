<article class="product-card">
    <div class="promotions">
        <div class="label discount">30% OFF</div>
    </div>
    <div class="product-img-wrapper">
        <a href="#">
            <img class="img-fluid" src="https://placehold.it/350x250&text=3-fallback" alt="Green Climbing Ivy Turnlock Edie Shoulder Bag">
        </a>
    </div>
    <div class="product-details">
        <h2 class="product-shop">Ounass</h2>
        <h3 class="product-name">
            <a href="#" title="Green Climbing Ivy Turnlock Edie Shoulder Bag">Green Climbing Ivy Turnlock Edie Shoulder Bag</a>
        </h3>
        <div class="actions">
            <div class="price-box">
                <div class="old-price">AED 2200.00</div>
                <div class="price">AED 1540.00</div>
            </div>
            <div class="button">
                <a href="#" title="Green Climbing Ivy Turnlock Edie Shoulder Bag" target="_blank" rel="nofollow">
                    <svg width="25px" height="32px" viewBox="0 0 25 32" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><title>Shop Deal Now</title><defs></defs><g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="shopping-bag" fill="#000000" fill-rule="nonzero"><path d="M24.9709302,27.0478036 L23.1750646,6.80878553 C23.1427649,6.40826873 22.8068475,6.10465116 22.3998708,6.10465116 L18.6143411,6.10465116 C18.6078811,2.73901809 15.868863,-2.84217094e-14 12.50323,-2.84217094e-14 C9.1375969,-2.84217094e-14 6.39857881,2.73901809 6.39211886,6.10465116 L2.60658915,6.10465116 C2.20607235,6.10465116 1.87015504,6.40826873 1.83139535,6.80878553 L0.0355297158,27.0478036 C0.0355297158,27.0736434 0.0355297158,27.0930233 0.0355297158,27.118863 C0.0355297158,29.373385 2.10917313,31.2080103 4.65439276,31.2080103 L20.3520672,31.2080103 C22.8972868,31.2080103 24.9709302,29.373385 24.9709302,27.118863 C24.9709302,27.0930233 24.9709302,27.0736434 24.9709302,27.0478036 Z M12.50323,1.5503876 C15.0161499,1.5503876 17.0574935,3.59173127 17.0639535,6.10465116 L7.94250646,6.10465116 C7.94896641,3.59173127 9.99031008,1.5503876 12.50323,1.5503876 Z M20.3520672,29.6511628 L4.65439276,29.6511628 C2.9748062,29.6511628 1.60529716,28.5335917 1.58591731,27.1447028 L3.31718346,7.65503876 L6.39211886,7.65503876 L6.39211886,10.374677 C6.39211886,10.8010336 6.74095607,11.1498708 7.16731266,11.1498708 C7.59366925,11.1498708 7.94250646,10.8010336 7.94250646,10.374677 L7.94250646,7.65503876 L17.0639535,7.65503876 L17.0639535,10.374677 C17.0639535,10.8010336 17.4127907,11.1498708 17.8391473,11.1498708 C18.2655039,11.1498708 18.6143411,10.8010336 18.6143411,10.374677 L18.6143411,7.65503876 L21.6892765,7.65503876 L23.4205426,27.1511628 C23.4011628,28.5335917 22.0316537,29.6511628 20.3520672,29.6511628 Z" id="Shape"></path></g></g></svg>
                </a>
            </div>
        </div>
    </div>
</article>