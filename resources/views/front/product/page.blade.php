@extends('layouts.front.product',[
    'pageTitle' => $product->meta_title,
    'metaDescription' => $product->meta_description,
    'metaKeywords' => $product->meta_keywords,
    'canonical' => URL::current(),
    'image' => $product->cover,
    'canonical' => route('front.product.slug',['slug' => $product->slug])
])

@section('content_top')
    {{ Breadcrumbs::render('front.product.slug', $product) }}
    <div class="pdp-header__title">
        <h1 class="pdp-header__title--name">{{ $product->name }}</h1>
        <div class="pdp-header__meta">
            <div class="pdp-header__meta-item">
                Brand: <a class="brand-name" href="{{ route('front.get.brand', str_slug($product->brand->slug)) }}" title="Compare {{ $product->brand->name }}">{{ $product->brand->name }}</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-7">
            <div class="pdp-header__image">
                <div class="pdp-header__image--thumb" data-slider-id="1">
                    <div class="image__thumb--item">
                        @if(strpos($product->cover, 'http') === 0)
                            <img src="{{ $product->cover }}" alt="{{ $product->name }}" class="image__thumb--img" title="Compare {{ $product->name }}">
                        @else
                            <img class="image__thumb--img" src="{{ URL::asset("imagecache/smallimage/$product->cover")}}" alt="{{ $product->name }}" title="Compare {{ $product->name }}">
                        @endif
                    </div>
                    @foreach($images as $image)
                        <div class="image__thumb--item">
                            <img class="image__thumb--img" src="{{ URL::asset("imagecache/smallimage/$image->src")}}" alt="{{ $product->name }}" title="Compare {{ $product->name }}">
                        </div>
                    @endforeach
                </div>
                <div class="owl-carousel pdp-header__image--main" data-slider-id="1">
                    <div class="item">
                        @include('front.product.includes.product-image')
                    </div>
                    @foreach($images as $image)
                        <div class="item" >
                            <img class="img-responsive img-thumbnail" src="{{ URL::asset("imagecache/mainimage/$image->src")}}" alt="{{ $product->name }}" title="Compare {{ $product->name }}">
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="col-md-5">
            <div class="pdp-header__offers">
                <h2 class="pdp-header__offers--title">Top offers to buy this product from</h2>
                <ul class="pdp-offers__shops">

                    @if(isset($product->productshop))
                        @foreach($product->productshop as $productshop)
                            <li class="pdp-offers__shops--item">
                                <div class="pdp-offers__shops--logo flex-fill">
                                    @foreach($productshop->shop as $shop)
                                        <img src="{{ asset("storage/$shop->logo") }}" alt="{{ $shop->name }}" class="img-fluid" width="100">
                                    @endforeach
                                </div>
                                <div class="pdp-offers__shops--price flex-fill">
                                    @if($productshop->special_price > '0.0')
                                        @php(
                                            $price = $productshop->special_price + $productshop->delivery_cost
                                        )
                                    @else
                                        @php(
                                            $price = $productshop->original_price + $productshop->delivery_cost
                                        )
                                    @endif
                                    <strong class="d-block mb-0 price"><span>AED</span> {{ $price }}</strong>
                                </div>
                                <div class="pdp-offers__shops--btn flex-fill">
                                    <a class="btn btn-secondary text-white d-block btn-sm" href="{{ $productshop->url }}" target="_blank" rel="nofollow">Go to Shop</a>
                                </div>
                            </li>
                        @endforeach
                    @endif
                </ul>
            </div>
        </div>
    </div>
@endsection

@section('content')

    <div class="row">
        <div class="col-sm-7">
            <h3 class="mb-4 h4">Compare Product Prices</h3>
        </div>
    </div>

    <div class="compare-block">
        @include('front.product.includes.compare')
    </div>

    <div class="description">
        {!! $product->description !!}
    </div>

@endsection

@section('footerjs')
  <script>
      $().ready(function () {
          $('.owl-carousel').owlCarousel({
              autoplay: false,
              loop: true,
              items: 1,
              center: false,
              nav: false,
              thumbs: true,
              thumbImage: false,
              thumbsPrerendered: true,
              thumbContainerClass: 'pdp-header__image--thumb',
              thumbItemClass: 'image__thumb--item'
          });
      });
  </script>
@endsection
