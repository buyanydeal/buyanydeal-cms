<h1>{{ $product->name }}</h1><!--name-->
<p>{!! $product->content !!}</p> <!--Description-->
    @php( $shop_data = $product->productshop->where('lowest_price', $product->productshop->min('lowest_price'))->first())

<div class="product-info">
    <dl class="row">
        <dt class="col-6">SKU</dt>
        <dd class="col-6 text-right">{{ $product->sku }}</dd>
        <dt class="col-6">Brand</dt>
        <dd class="col-6 text-right">{{ $product->brand->name }}</dd>
        <dt class="col-6">Delivery Cost</dt>
        <dd class="col-6 text-right">{{ $shop_data['delivery_cost'] }} AED</dd>
        <dt class="col-6">Delivery Time</dt>
        <dd class="col-6 text-right">{{ $shop_data['delivery_time'] }}</dd>
        @if(isset($shop_data))
            @foreach ($shop_data->shop as $shop)
                <dt class="col-6">Shop</dt>
                <dd class="col-6 text-right">{{ $shop->name }}</dd>
            @endforeach
        @endif
    </dl>
</div>
<div class="row">
    <div class="col-md-6">
        @if($shop_data['special_price'] > '0.01')
            <div class="orginal_price">AED {{ $shop_data['original_price'] }}</div>
            <div class="special_price">AED {{ $shop_data['special_price'] }}</div>
        @else
            <div class="special_price">AED {{ $shop_data['original_price'] }}</div>
        @endif
    </div>
    <div class="col-md-6 text-right">
        <a href="{{ $shop_data['url'] }}" class="btn btn-secondary btn-lg text-white" target="_blank" rel="nofollow">View product</a>
    </div>
</div>