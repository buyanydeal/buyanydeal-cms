<ul class="p-0">
    @if(isset($product->productshop))
        @foreach($product->productshop as $productshop)
            <li class="position-relative bg-white">
                <div class="row">
                    <div class="col-sm-8">
                        <div class="row">
                            <div class="col-sm-3">
                                @foreach($productshop->shop as $shop)
                                    <img src="{{ asset("storage/$shop->logo") }}" alt="{{ $shop->name }}" class="img-fluid">
                                    <div class="rate text-center">
                                        <i class="fa fa-star pr-1 pt-2 pb-0 text-muted"></i><i class="fa fa-star pr-1 pt-2 pb-0 text-muted"></i><i class="fa fa-star pr-1 pt-2 pb-0 text-muted"></i><i class="fa fa-star pr-1 pt-2 pb-0 text-muted"></i><i class="fa fa-star pr-1 pt-2 pb-0 text-muted"></i>
                                    </div>
                                @endforeach
                            </div>
                            <div class="col-sm-9 product-details">
                                <strong class="d-block mb-3">{{ $product->name }}</strong>
                                <small class="d-block"><i class="fa fa-truck pb-2 text-muted"></i>{{ $productshop->delivery_time }}</small>
                                <small class="d-block"><i class="fas fa-coins text-muted"></i>AED {{ $productshop->delivery_cost }} Shipping</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="d-flex align-items-center h-100">
                            <div>
                                <strong class="d-block mb-0 price">
                                    @if($productshop->special_price > '0.0')
                                        @php(
                                            $price = $productshop->special_price + $productshop->delivery_cost
                                        )
                                    @else
                                        @php(
                                            $price = $productshop->original_price + $productshop->delivery_cost
                                        )
                                    @endif
                                    <span>AED</span>  {{ $price }}
                                </strong>
                                <small class="text-muted">Including shipping cost</small>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="d-flex align-items-center h-100 float-right">
                            <div>
                                {{--@include('front.product.includes.coupon')--}}
                                <a href="{{ $productshop->url }}" class="btn btn-secondary text-white d-block" target="_blank" rel="nofollow">View product</a>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
        @endforeach
    @endif
</ul>