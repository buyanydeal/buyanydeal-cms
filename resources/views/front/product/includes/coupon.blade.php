<!-- Button trigger modal -->
<button type="button" class="btn" data-toggle="modal" data-target="#exampleModalCenter">
    Get Coupon
</button>
<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-white" id="exampleModalLongTitle">Sprii Coupon code</h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <input type="text" value="BADeal398" id="myInput" class="coupon-code text-center mb-4 text-primary">
                <p>Use coupon <strong>"BADeal398"</strong> for 50 AED off on first purchase using Axiom app for orders above AED 699.</p>


                <div class="tooltip">
                    <button onclick="myFunction()" onmouseout="outFunc()">
                        <span class="tooltiptext" id="myTooltip">Copy to clipboard</span>
                        Copy text
                    </button>
                </div>
                <button onclick="myFunction()" class="copy btn btn-light"><i class="fa fa-copy mr-2"></i>Copy Code</button>
                <!-- copy the coupon code -->
                <script>
                    function myFunction() {
                        var copyText = document.getElementById("myInput");
                        copyText.select();
                        document.execCommand("copy");
                    }
                </script>
                <hr>
                20 people used this coupon <br/>
                expires on 29/07/2017
                <a href="{{ $product->url }}" class="btn btn-secondary">View product</a>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>