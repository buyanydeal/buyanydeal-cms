@if(strpos($product->cover, 'http') === 0)
    <img src="{{ $product->cover }}" alt="{{ $product->name }}" class="img-fluid product-cover" title="Compare {{ $product->name }}">
@else
    <img class="img-fluid product-cover" src="{{ URL::asset("imagecache/mainimage/$product->cover")}}" alt="{{ $product->name }}" title="Compare {{ $product->name }}">
@endif
{{--<div class="discount-percentage position-absolute text-white">30% Off</div>--}}