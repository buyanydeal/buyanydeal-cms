@extends('layouts.front.app-sidebar',[
    'pageTitle' => 'Search',
    'metaDescription' => 'Search',
    'metaKeywords' => 'Search',
    'canonical' => ''
])

@section('sidebar')
@endsection

@section('content')
    <div class="category-nav-top">

        <div class="toolbar d-flex justify-content-between">
            <div class="product-count my-2">
                {{ $products->total() }} @if($products->total() == 1)Item @else Items @endif
            </div>
            <nav class="justify-content-end" aria-label="category navigation">
                @if($products instanceof \Illuminate\Contracts\Pagination\LengthAwarePaginator)
                    <div class="row">
                        <div class="col-md-12">
                            <div class="pull-left">{{ $products->links() }}</div>
                        </div>
                    </div>
                @endif
            </nav>
        </div>


        <div class="product-list">
            <div class="row">
                @foreach($products as $product)
                    <div class="col-6 col-md-4 col-lg-3">

                        @include('front.widgets.products.product-block', [
                                            'product' => $product,
                                            'brand' => $product->brand->name,
                                            'cover' => $product->cover,
                                            'name' => $product->name,
                                            'slug' => $product->slug
                                        ])
                    </div>
                @endforeach
            </div>
        </div>

        <div class="toolbar d-flex justify-content-end">
            <nav class="justify-content-end" aria-label="category navigation">
                @if($products instanceof \Illuminate\Contracts\Pagination\LengthAwarePaginator)
                    {{ $products->links() }}
                @endif
            </nav>
        </div>
    </div>
@endsection

@section('footerjs')
@endsection