@extends('layouts.front.app',[
    'pageTitle' => $shop->meta_title,
    'metaDescription' => $shop->meta_description,
    'metaKeywords' => $shop->meta_keywords,
    'canonical' => route('front.shop.show', str_slug($shop->slug))
])

@section('before-content')
    <div class="content-header">
        <div class="row">
            <div class="col-md-3">
                <div class="back-link">
                    <a href="{{ route('front.shop.index')}}">
                        <i class="fa fa-chevron-left"></i> {{ __('brands.shops-link') }}
                    </a>
                </div>
            </div>
            <div class="col-md-9">
{{--                {{ Breadcrumbs::render('front.shop.show', $shop) }}--}}
            </div>
        </div>
    </div>
@endsection

@section('sidebar')

@endsection

@section('content')
    <h1 class="h2">Compare {{ $shop->name }} products</h1>
    @include('front.widgets.header.page-header', [
        'cover' => $shop->logo,
        'name' => $shop->name,
        'description' => $shop->description
    ])
@endsection