@extends('layouts.front.app-sidebar',[
    'pageTitle' => 'Compare brands - #1 online shopping mall in UAE | buyanydeal.com',
    'metaDescription' => 'Price comparison for these brands with all online stores in the UAE. Buyanydeal is your Dubai Mall Online. √ Easy to search √ easy to filter',
    'metaKeywords' => 'online shopping, comparison, uae, dubai, Abu Dhabi, Al Ain, Sharjah, buy online, cheapest price',
    'canonical' => route('front.shop.index')
])

@section('head-script')
    {{ Breadcrumbs::view('breadcrumbs::json-ld', 'front.shop.index') }}
@endsection

@section('before-content')
    <div class="content-header">
        <div class="row">
            <div class="col-md-3">
                <div class="back-link">
                    <a href="{{ route('front.home.index')}}">
                        <i class="fa fa-chevron-left"></i> {{ __('shops.backlink') }}
                    </a>
                </div>
            </div>
            <div class="col-md-9">
                {{ Breadcrumbs::render('front.shop.index') }}
            </div>
        </div>
    </div>
@endsection

@section('sidebar')
@endsection

@section('content')
    <h1 class="h2 page-title">Compare Brands in the United Arab Emirates</h1>

    <ul class="brand-index">
        @foreach($sortedShops as $letter => $shops)
            <li><a href="#{{ $letter }}">{{ $letter }}</a></li>
        @endforeach
    </ul>

    @foreach($sortedShops as $letter => $shops)
        <h2 id="{{ $letter }}" class="h5">{{ $letter }}</h2>
        <ul class="brand-list">
            @foreach($shops as $shop)
                <li>
                    <a href="{{ route('front.shop.show', str_slug($shop->slug)) }}" title="{{ $shop->name }}">
                        {{ $shop->name }}
                    </a>
                </li>
            @endforeach
        </ul>
    @endforeach
@endsection