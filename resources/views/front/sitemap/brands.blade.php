<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <url>
        <loc>https://buyanydeal.com/compare-brands</loc>
        <lastmod>{{ $lastupdate->created_at->tz('UTC')->toAtomString() }}</lastmod>
        <changefreq>weekly</changefreq>
        <priority>0.6</priority>
    </url>
    @foreach ($brands as $brand)
        <url>
            <loc>https://buyanydeal.com/compare-brands/{{ $brand->slug }}.html</loc>
            <lastmod>{{ $brand->created_at->tz('UTC')->toAtomString() }}</lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.6</priority>
        </url>
    @endforeach
</urlset>