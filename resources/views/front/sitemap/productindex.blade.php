<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    @foreach ($categories as $category)
        <sitemap>
            <loc>https://buyanydeal.com/sitemap/products-{{ $category->slug }}.xml</loc>
            <lastmod>{{ $lastupdate->created_at->tz('UTC')->toAtomString() }}</lastmod>
        </sitemap>
    @endforeach
</sitemapindex>