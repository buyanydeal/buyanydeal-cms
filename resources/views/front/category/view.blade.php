@if(isset($category->meta_title))
    @php($meta_title = $category->meta_title)
    @php($meta_description = $category->meta_title)
    @php($meta_keywords = $category->meta_keywords)
@elseif(isset($category->page_title))
    @php($meta_title = 'Compare ' . $category->page_title . ' Prices online in the UAE | Buyanydeal')
    @php($meta_description = 'Price comparison for ' . $category->page_title . ' with all online stores in the UAE. Buyanydeal is your Dubai Mall Online. √ Easy to search √ easy to filter')
    @php($meta_keywords = $category->page_title . ', ' . $category->name)
@else
    @php($meta_title = 'Compare ' . $category->name . ' Prices online in the UAE | Buyanydeal')
    @php($meta_description = 'Price comparison for ' . $category->name . ' with all online stores in the UAE. Buyanydeal is your Dubai Mall Online. √ Easy to search √ easy to filter')
    @php($meta_keywords = $category->name)
@endif

@extends('layouts.front.app-sidebar',[
    'pageTitle' => $meta_title,
    'metaDescription' => $meta_description,
    'metaKeywords' => $meta_keywords,
    'canonical' => route('front.category.view', str_slug($category->slug))
])

@section('sidebar')
    <div class="secondary-nav">
        @if(!$category->children->isEmpty())
            <div class="block block-nav-category">
                <div class="block-title">
                    <strong class="h6">Categories</strong>
                </div>
                <div class="block-content">
                    <ul>
                        @foreach($category->children as $children)
                            <li>
                                <a href="{{ route('front.category.view', str_slug($children->slug)) }}" title="{{ $children->name }}">{{ $children->name }}</a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        @endif
    </div>
@endsection

@section('content')
    @include('front.category.includes.header')
    <div class="category-nav-top">
        <div class="toolbar d-flex justify-content-between">
            <div class="product-count my-2">
                {{ $products->total() }} @if($products->total() == 1)Item @else Items @endif
            </div>
            <nav class="justify-content-end" aria-label="category navigation">
                @include('layouts.shared.pagination', ['paginator' => $products])
            </nav>
        </div>

        @include('front.category.includes.product-list', ['products' => $products])

        <div class="toolbar d-flex justify-content-end">
            <nav class="justify-content-end" aria-label="category navigation">
                @include('layouts.shared.pagination', ['paginator' => $products])
            </nav>
        </div>
    </div>
    @if(isset($category->seo_description))
        <div class="seo-desc">
            {!! $category->seo_description !!}
        </div>
    @endif
@endsection

@section('footerjs')
    <script type="application/ld+json">
        {
            "@context": "http://schema.org",
            "@type": "WebPage",
            "breadcrumb": "Home",
            "name": "Online shopping mall in UAE | Buyanydeal",
            "url": "https://buyanydeal.com/",
            "description": "Price comparison with all online stores in the UAE. Buyanydeal is your Dubai Mall Online. Online shopping in the UAE is fun!",
            "publisher": {
                "@type": "Organization",
                "name": "Buyanydeal"
            },
             "potentialAction": [
             {
                "@type": "SearchAction",
                "target": "https://buyanydeal.com/search?q={search_term_string}",
                "query-input": "required name=search_term_string"
             }]
        }
    </script>
@endsection
