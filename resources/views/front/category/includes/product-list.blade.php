{{--@php(dd($products))--}}

@if(!empty($products) && !collect($products)->isEmpty())
<div class="product-list">
    <div class="row">
        @foreach($products as $product)
            {{ $product->test }}
            <div class="col-6 col-md-4 col-lg-3">
                <article class="product-card">
                    @foreach($product->productshop as $shop)
                        @if($shop->special_price != '0.0')
                            @php($percentage = round((($shop->original_price - $shop->special_price) / $shop->original_price) * 100))
                            <div class="promotions">
                                <div class="label discount">{{ $percentage }}% cheaper</div>
                            </div>
                        @endif
                    @endforeach
                    <div class="product-img-wrapper">
                        <a href="{{route('front.product.slug',['slug' => $product->slug])}}">
                            @if(strpos($product->cover, 'http') === 0)
                                <img class="img-fluid" src="{{ $product->cover }}" alt="{{ $product->name }}" title="Compare {{ $product->name }}">
                            @else
                                <img src="{{ URL::asset("imagecache/thumbnail/$product->cover")}}" alt="{{ $product->name }}" title="Compare {{ $product->name }}">
                            @endif
                        </a>
                    </div>
                    <div class="product-details">
                        <h2 class="product-shop text-capitalize">
                            @if(isset($product->brand))
                                {{ $product->brand->name }}
                            @endif
                        </h2>
                        <h3 class="product-name">
                            <a href="{{route('front.product.slug',['slug' => $product->slug])}}">
                                {{ $product->name }}
                            </a>
                        </h3>
                        <div class="actions">
                            <div class="price-box">
                                @foreach($product->productshop as $shop)

                                    @if($shop->special_price != '0.0')
                                        <div class="old-price">AED {{ $shop->original_price }}</div>
                                        <div class="price">AED {{ $shop->special_price }}</div>
                                    @else
                                        <div class="old-price lowest">Lowest price:</div>
                                        <div class="price">AED {{ $shop->original_price }}</div>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        @endforeach
    </div>
</div>
@else
    <p class="alert alert-warning">No products yet.</p>
@endif