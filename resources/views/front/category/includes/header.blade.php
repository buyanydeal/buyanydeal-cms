@if(isset($category->page_title))
    <h1 class="page-title h3 mb-3 text-capitalize">{{ $category->page_title }}</h1>
@else
    <h1 class="page-title h3 mb-3 text-capitalize">{{ $category->name }}</h1>
@endif
@if($category->cover !== NULL && $category->description !== NULL)
    <div class="category-header d-none d-md-block d-flex mb-2">
        <div class="row align-items-center">
            <div class="col-md-6">
                <img class="img-fluid" src="{{ asset("storage/$category->cover") }}" alt="{{ $category->name }} Product Comparison" title="{{ $category->name }}">
            </div>
            <div class="col-md-6 text-center">
                {!! $category->description !!}
            </div>
        </div>
    </div>
@endif