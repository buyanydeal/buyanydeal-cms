@extends('layouts.front.app',[
    'pageTitle' => 'All categories - #1 online shopping mall in UAE | buyanydeal.com',
    'metaDescription' => 'Price comparison for these categories with all online stores in the UAE. Buyanydeal is your Dubai Mall Online. √ Easy to search √ easy to filter',
    'metaKeywords' => 'online shopping, comparison, uae, dubai, Abu Dhabi, Al Ain, Sharjah, buy online, cheapest price',
    'canonical' => route('front.category.index')
])

@section('content')
    <h1 class="page-title h3 mb-3">Compare prices by category</h1>
    <ul class="category-sitemap row">
        <li class="col-12 col-md-4">
            <h2 class="h5"><a href="#" title="">Women</a></h2>
            <ul class="level-1 list-group">
                <li class="list-group-item">
                    <a href="#" title="">Clothes</a>
                </li>
                <li class="list-group-item">
                    <a href="#" title="">Plus Size Clothing</a>
                </li>
                <li class="list-group-item">
                    <a href="#" title="">Lingerie & Sleepwear</a>
                </li>
                <li class="list-group-item">
                    <a href="#" title="">Beachwear & Swimwear</a>
                </li>
                <li class="list-group-item">
                    <a href="#" title="">Shoes</a>
                </li>
                <li class="list-group-item">
                    <a href="#" title="">Accessories</a>
                </li>
                <li class="list-group-item">
                    <a href="#" title="">Bags</a>
                </li>
                <li class="list-group-item">
                    <a class="btn btn-outline-primary btn-sm" href="#" title="">All Women</a>
                </li>
            </ul>
        </li>
        <li class="col-12 col-md-4">
            <h2 class="h5"><a href="#" title="">Men</a></h2>
            <ul class="level-1 list-group">
                <li class="list-group-item">
                    <a href="#" title="">Clothes</a>
                </li>
                <li class="list-group-item">
                    <a href="#" title="">Plus Size Clothing</a>
                </li>
                <li class="list-group-item">
                    <a href="#" title="">Underwear & Sleepwear</a>
                </li>
                <li class="list-group-item">
                    <a href="#" title="">Swimwear</a>
                </li>
                <li class="list-group-item">
                    <a href="#" title="">Shoes</a>
                </li>
                <li class="list-group-item">
                    <a href="#" title="">Bags</a>
                </li>
                <li class="list-group-item">
                    <a href="#" title="">Accessories</a>
                </li>
                <li class="list-group-item">
                    <a class="btn btn-outline-primary btn-sm" href="#" title="">All Men</a>
                </li>
            </ul>
        </li>
        <li class="col-12 col-md-4">
            <h2 class="h5"><a href="#" title="">Baby & Kids</a></h2>
            <ul class="level-1 list-group">
                <li class="list-group-item">
                    <a href="#" title="">Clothes</a>
                </li>
                <li class="list-group-item">
                    <a href="#" title="">Underwear & Sleepwear</a>
                </li>
                <li class="list-group-item">
                    <a href="#" title="">Beachwear & Swimwear</a>
                </li>
                <li class="list-group-item">
                    <a href="#" title="">Shoes</a>
                </li>
                <li class="list-group-item">
                    <a href="#" title="">Accessories</a>
                </li>
                <li class="list-group-item">
                    <a href="#" title="">Bags</a>
                </li>
                <li class="list-group-item">
                    <a href="#" title="">Furniture</a>
                </li>
                <li class="list-group-item">
                    <a class="btn btn-outline-primary btn-sm" href="#" title="">All Baby & Child</a>
                </li>
            </ul>
        </li>
        <li class="col-12 col-md-4">
            <h2 class="h5"><a href="#" title="">Mobiles, Tablets & Wearables</a></h2>
            <ul class="level-1 list-group">
                <li class="list-group-item">
                    <a href="#" title="">Clothes</a>
                </li>
                <li class="list-group-item">
                    <a class="btn btn-outline-primary btn-sm" href="#" title="">All Mobiles, Tablets & Wearables</a>
                </li>
            </ul>
        </li>
        <li class="col-12 col-md-4">
            <h2 class="h5"><a href="#" title="">Computers</a></h2>
            <ul class="level-1 list-group">
                <li class="list-group-item">
                    <a href="#" title="">Clothes</a>
                </li>
                <li class="list-group-item">
                    <a class="btn btn-outline-primary btn-sm" href="#" title="">All Computers</a>
                </li>
            </ul>
        </li>
        <li class="col-12 col-md-4">
            <h2 class="h5"><a href="#" title="">TV & Audio</a></h2>
            <ul class="level-1 list-group">
                <li class="list-group-item">
                    <a href="#" title="">Clothes</a>
                </li>
                <li class="list-group-item">
                    <a class="btn btn-outline-primary btn-sm" href="#" title="">All Computers</a>
                </li>
            </ul>
        </li>
        <li class="col-12 col-md-4">
            <h2 class="h5"><a href="#" title="">Camera's</a></h2>
            <ul class="level-1 list-group">
                <li class="list-group-item">
                    <a href="#" title="">Clothes</a>
                </li>
                <li class="list-group-item">
                    <a class="btn btn-outline-primary btn-sm" href="#" title="">All Computers</a>
                </li>
            </ul>
        </li>
        <li class="col-12 col-md-4">
            <h2 class="h5"><a href="#" title="">Consoles & Games</a></h2>
            <ul class="level-1 list-group">
                <li class="list-group-item">
                    <a href="#" title="">Clothes</a>
                </li>
                <li class="list-group-item">
                    <a class="btn btn-outline-primary btn-sm" href="#" title="">All Computers</a>
                </li>
            </ul>
        </li>
        <li class="col-12 col-md-4">
            <h2 class="h5"><a href="#" title="">Office Supplies</a></h2>
            <ul class="level-1 list-group">
                <li class="list-group-item">
                    <a href="#" title="">Clothes</a>
                </li>
                <li class="list-group-item">
                    <a class="btn btn-outline-primary btn-sm" href="#" title="">All Computers</a>
                </li>
            </ul>
        </li>
        <li class="col-12 col-md-4">
            <h2 class="h5"><a href="#" title="">Home & Garden</a></h2>
            <ul class="level-1 list-group">
                <li class="list-group-item">
                    <a href="#" title="">Clothes</a>
                </li>
                <li class="list-group-item">
                    <a class="btn btn-outline-primary btn-sm" href="#" title="">All Computers</a>
                </li>
            </ul>
        </li>
        <li class="col-12 col-md-4">
            <h2 class="h5"><a href="#" title="">Health & Beauty</a></h2>
            <ul class="level-1 list-group">
                <li class="list-group-item">
                    <a href="#" title="">Clothes</a>
                </li>
                <li class="list-group-item">
                    <a class="btn btn-outline-primary btn-sm" href="#" title="">All Computers</a>
                </li>
            </ul>
        </li>
        <li class="col-12 col-md-4">
            <h2 class="h5"><a href="#" title="">Home Appliances</a></h2>
            <ul class="level-1 list-group">
                <li class="list-group-item">
                    <a href="#" title="">Clothes</a>
                </li>
                <li class="list-group-item">
                    <a class="btn btn-outline-primary btn-sm" href="#" title="">All Computers</a>
                </li>
            </ul>
        </li>
        <li class="col-12 col-md-4">
            <h2 class="h5"><a href="#" title="">Car Parts & Accessories</a></h2>
            <ul class="level-1 list-group">
                <li class="list-group-item">
                    <a href="#" title="">Clothes</a>
                </li>
                <li class="list-group-item">
                    <a class="btn btn-outline-primary btn-sm" href="#" title="">All Computers</a>
                </li>
            </ul>
        </li>
        <li class="col-12 col-md-4">
            <h2 class="h5"><a href="#" title="">Sport</a></h2>
            <ul class="level-1 list-group">
                <li class="list-group-item">
                    <a href="#" title="">Clothes</a>
                </li>
                <li class="list-group-item">
                    <a class="btn btn-outline-primary btn-sm" href="#" title="">All Computers</a>
                </li>
            </ul>
        </li>
        <li class="col-12 col-md-4">
            <h2 class="h5"><a href="#" title="">Books</a></h2>
            <ul class="level-1 list-group">
                <li class="list-group-item">
                    <a href="#" title="">Clothes</a>
                </li>
                <li class="list-group-item">
                    <a class="btn btn-outline-primary btn-sm" href="#" title="">All Computers</a>
                </li>
            </ul>
        </li>
        <li class="col-12 col-md-4">
            <h2 class="h5"><a href="#" title="">Arts & Crafts</a></h2>
            <ul class="level-1 list-group">
                <li class="list-group-item">
                    <a href="#" title="">Clothes</a>
                </li>
                <li class="list-group-item">
                    <a class="btn btn-outline-primary btn-sm" href="#" title="">All Computers</a>
                </li>
            </ul>
        </li>
        <li class="col-12 col-md-4">
            <h2 class="h5"><a href="#" title="">Travel</a></h2>
            <ul class="level-1 list-group">
                <li class="list-group-item">
                    <a href="#" title="">Clothes</a>
                </li>
                <li class="list-group-item">
                    <a class="btn btn-outline-primary btn-sm" href="#" title="">All Computers</a>
                </li>
            </ul>
        </li>
        <li class="col-12 col-md-4">
            <h2 class="h5"><a href="#" title="">Music</a></h2>
            <ul class="level-1 list-group">
                <li class="list-group-item">
                    <a href="#" title="">Clothes</a>
                </li>
                <li class="list-group-item">
                    <a class="btn btn-outline-primary btn-sm" href="#" title="">All Computers</a>
                </li>
            </ul>
        </li>
        <li class="col-12 col-md-4">
            <h2 class="h5"><a href="#" title="">Computers</a></h2>
            <ul class="level-1 list-group">
                <li class="list-group-item">
                    <a href="#" title="">Clothes</a>
                </li>
                <li class="list-group-item">
                    <a class="btn btn-outline-primary btn-sm" href="#" title="">All Computers</a>
                </li>
            </ul>
        </li>
        <li class="col-12 col-md-4">
            <h2 class="h5"><a href="#" title="">Groceries</a></h2>
            <ul class="level-1 list-group">
                <li class="list-group-item">
                    <a href="#" title="">Clothes</a>
                </li>
                <li class="list-group-item">
                    <a class="btn btn-outline-primary btn-sm" href="#" title="">All Computers</a>
                </li>
            </ul>
        </li>
        <li class="col-12 col-md-4">
            <h2 class="h5"><a href="#" title="">Pet Supplies</a></h2>
            <ul class="level-1 list-group">
                <li class="list-group-item">
                    <a href="#" title="">Dog Supplies</a>
                </li>
                <li class="list-group-item">
                    <a href="#" title="">Cat Supplies</a>
                </li>
                <li class="list-group-item">
                    <a href="#" title="">Bird supplies</a>
                </li>
                <li class="list-group-item">
                    <a href="#" title="">Amphibians & More</a>
                </li>
                <li class="list-group-item">
                    <a href="#" title="">Small Pets</a>
                </li>
                <li class="list-group-item">
                    <a href="#" title="">Equestrian</a>
                </li>
                <li class="list-group-item">
                    <a href="#" title="">Aquatics</a>
                </li>
                <li class="list-group-item">
                    <a class="btn btn-outline-primary btn-sm" href="#" title="">All Computers</a>
                </li>
            </ul>
        </li>
    </ul>
@endsection

@section('footerjs')
@endsection