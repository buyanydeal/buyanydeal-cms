@extends('layouts.front.app',[
    'pageTitle' => 'Online shopping mall in UAE | Buyanydeal',
    'metaDescription' => 'Compare prices in the online shopping mall in the UAE. Buyanydeal is makes online shopping fun and cheaper. Read reviews and compare.',
    'metaKeywords' => 'online shopping, comparison, uae, dubai, Abu Dhabi, Al Ain, Sharjah, buy online, cheapest price',
    'canonical' => route('front.home.index')
])



@section('content')

    @include('front.widgets.slider.slider')

    @include('front.widgets.products.viewed', [
        'title' => 'Compare TV products',
        'products' => $tvProducts,
        'route' => route('front.category.view',['slug' => 'tv-audio-televisions'])
    ])

    @include('front.widgets.products.carousel', [
        'title' => 'Compare Women Products',
        'products' => $womenProducts,
        'route' => route('front.category.view',['slug' => 'women-fashion'])
    ])

    @include('front.widgets.products.carousel', [
        'title' => 'Compare Men Products',
        'products' => $menProducts,
        'route' => route('front.category.view',['slug' => 'men-fashion'])
    ])

    @include('front.widgets.products.carousel', [
        'title' => 'Compare Baby & Kids Products',
        'products' => $babyKidsProducts,
        'route' => route('front.category.view',['slug' => 'baby-kids'])
    ])


    {{--@include('front.widgets.categories.category-thumbs')--}}

    {{--@include('front.widgets.shops.shops-slider')--}}

    {{--@include('front.widgets.products.products-slider')--}}

    {{--@include('front.widgets.brands.brands-slider')--}}

    <div class="row">
        <div class="col-12">
          <h1 class="text-center mb-3 mt-3 h2">Buyanydeal is your online shopping mall in UAE</h1>
          <p class="text-center">Buyanydeal provides a better experience while shopping online in the UAE.<br> Buyanydeal is your online shopping mall in the UAE.</p>
        </div>
    </div>

    <div class="row">
        <div class="col-12 col-sm-6 col-md-4 col-lg-3 cats__widget">
            <h2 class="h6 cats__widget--title">Women</h2>
            <ul class="catslist">
                <li class="catlist__item">
                    <a href="{{route('front.category.view',['slug' => 'women-fashion'])}}" title="Women Clothes" class="catlist__item--link">Woman Clothes</a>
                </li>
                <li class="catlist__item">
                    <a href="{{route('front.category.view',['slug' => 'women-fashion-plus-size'])}}" title="Women Plus Size Clothing" class="catlist__item--link">Plus Size Clothing</a>
                </li>
                <li class="catlist__item">
                    <a href="{{route('front.category.view',['slug' => 'women-fashion-lingerie-and-sleepwear'])}}" title="Women Lingerie & Sleepwear" class="catlist__item--link">Lingerie & Sleepwear</a>
                </li>
                <li class="catlist__item">
                    <a href="{{route('front.category.view',['slug' => 'women-fashion-beachwear-swimwear'])}}" title="Women Beachwear & Swimwear" class="catlist__item--link">Beachwear & Swimwear</a>
                </li>
                <li class="catlist__item">
                    <a href="{{route('front.category.view',['slug' => 'women-fashion-shoes'])}}" title="Women Shoes" class="catlist__item--link">Woman Shoes</a>
                </li>
                <li class="catlist__item">
                    <a href="{{route('front.category.view',['slug' => 'women-fashion-bags'])}}" title="Women Bags" class="catlist__item--link">Woman Bags</a>
                </li>
            </ul>
            <a class="btn btn-light btn-sm" href="{{route('front.category.view',['slug' => 'women-fashion'])}}" title="All Women Fashion">See All</a>
        </div>
        <div class="col-12 col-sm-6 col-md-4 col-lg-3 cats__widget">
            <h2 class="h6 cats__widget--title">Men</h2>
            <ul class="catslist">
                <li class="catlist__item">
                    <a href="{{route('front.category.view',['slug' => 'men-fashion-clothes'])}}" title="Men Clothes" class="catlist__item--link">Men Clothes</a>
                </li>
                <li class="catlist__item">
                    <a href="{{route('front.category.view',['slug' => 'men-fashion-plus-size-clothing'])}}" title="Men Plus Size Clothing" class="catlist__item--link">Plus Size Clothing</a>
                </li>
                <li class="catlist__item">
                    <a href="{{route('front.category.view',['slug' => 'men-fashion-underwear-sleepwear'])}}" title="Men Underwear & Sleepwear" class="catlist__item--link">Underwear & Sleepwear</a>
                </li>
                <li class="catlist__item">
                    <a href="{{route('front.category.view',['slug' => 'men-fashion-swimwear'])}}" title="Men Swimwear" class="catlist__item--link">Men Swimwear</a>
                </li>
                <li class="catlist__item">
                    <a href="{{route('front.category.view',['slug' => 'men-fashion-shoes'])}}" title="Men Shoes" class="catlist__item--link">Men Shoes</a>
                </li>
                <li class="catlist__item">
                    <a href="{{route('front.category.view',['slug' => 'men-fashion-bags'])}}" title="Men Bags" class="catlist__item--link">Men Bags</a>
                </li>
            </ul>
            <a class="btn btn-light btn-sm" href="{{route('front.category.view',['slug' => 'men-fashion'])}}" title="All Men Fashion">See All</a>
        </div>
        <div class="col-12 col-sm-6 col-md-4 col-lg-3 cats__widget">
            <h2 class="h6 cats__widget--title">Baby & Kids</h2>
              <ul class="catslist">
                  <li class="catlist__item">
                      <a href="{{route('front.category.view',['slug' => 'baby-kids-clothes'])}}" title="Baby & Kids Clothes" class="catlist__item--link">Clothes</a>
                  </li>
                  <li class="catlist__item">
                      <a href="{{route('front.category.view',['slug' => 'baby-kids-underwear-sleepwear'])}}" title="Baby & Kids Underwear & Sleepwear" class="catlist__item--link">Underwear & Sleepwear</a>
                  </li>
                  <li class="catlist__item">
                      <a href="{{route('front.category.view',['slug' => 'baby-kids-beachwear-swimwear'])}}" title="Baby & Kids Beachwear & Swimwear" class="catlist__item--link">Beachwear & Swimwear</a>
                  </li>
                  <li class="catlist__item">
                      <a href="{{route('front.category.view',['slug' => 'baby-kids-shoes'])}}" title="Baby & Kids Shoes" class="catlist__item--link">Shoes</a>
                  </li>
                  <li class="catlist__item">
                      <a href="{{route('front.category.view',['slug' => 'baby-kids-furniture'])}}" title="Baby & Kids Furniture" class="catlist__item--link">Furniture</a>
                  </li>
                  <li class="catlist__item">
                      <a href="{{route('front.category.view',['slug' => 'baby-kids-bathing'])}}" title="Baby & Kids Bathing" class="catlist__item--link">Bathing</a>
                  </li>
              </ul>
              <a class="btn btn-light btn-sm" href="{{route('front.category.view',['slug' => 'baby-kids'])}}" title="All baby & kids Fashion">See All</a>
          </div>
          <div class="col-12 col-sm-6 col-md-4 col-lg-3 cats__widget">
            <h2 class="h6 cats__widget--title">TV & Audio</h2>
              <ul class="catslist">
                  <li class="catlist__item">
                      <a href="{{route('front.category.view',['slug' => 'tv-audio-televisions'])}}" title="televisions" class="catlist__item--link">Televisions</a><span class="badge badge-secondary ml-2">top</span>
                  </li>
                  <li class="catlist__item">
                      <a href="{{route('front.category.view',['slug' => 'tv-audio-audio-video-accessories'])}}" title="Audio & Video Accessories" class="catlist__item--link">Audio & Video Accessories</a>
                  </li>
                  <li class="catlist__item">
                      <a href="{{route('front.category.view',['slug' => 'tv-audio-speakers'])}}" title="Speakers" class="catlist__item--link">Speakers</a>
                  </li>
                  <li class="catlist__item">
                      <a href="{{route('front.category.view',['slug' => 'tv-audio-headphones'])}}" title="Headphones" class="catlist__item--link">Headphones</a>
                  </li>
              </ul>
              <a class="btn btn-light btn-sm" href="{{route('front.category.view',['slug' => 'tv-audio'])}}" title="All tv & audio">See All</a>
          </div>
    </div>

@endsection

@section('footerjs')
    <div class="container after-footer">
        <div class="row">
          <div class="col-12 mb-3">
            <h2 class="text-center mb-3">Online Shopping in the UAE</h2>
            <p class="text-center">Online shopping in Dubai, Abu Dhabi or any other United Arab Emirates.<br> Whatever you are looking to buy:
              <a href="{{route('front.category.view',['slug' => 'women-fashion'])}}" title="Women Clothes" class="catlist__item--link">Woman Clothes</a>, mobile phones, accessories or any books, you can easily shop online in the UAE.</p>
          </div>
            <div class="col-12 col-md-6">
                <h3 class="h5">Compare prices online in the UAE</h3>
                <p>With buyanydeal you are able to compare prices online for the following categories: women fashion, men fashion, baby & kids essentials, game consoles, mobiles and tablets, computers, tv and audio, cameras and many more. In order to get the cheapest price available on the market, we show you a comparison of each webshop. To receive even a better discount you can use the coupon codes we provide with the products.  So, are you all set to head online and shop? Let's take a look at what buyanydeal has in store for you, shall we?</p>
            </div>
            <div class="col-12 col-md-6">
              <h3 class="h5">Buy Electronics Online at Attractive Prices</h3>
              <p>In the category of electronic products, it is very easy to save money while comparing prices. Many online shops try to attract customers by creating nice offers for a TV, Audio installation or mobile phones. At buyanydeal we offer you a selection of the best electronic products online with a high quality and great value. It is very easy to compare prices on thousands of products and read user and expert reviews to make the best purchase decisions.</p>
            </div>
            <div class="col-12 col-md-6">
              <h3 class="h5">Shopping TV and Home Cinema Sets online</h3>
              <p>Are you looking to upgrade your home electronic experience by buying a new TV and >Home Cinema Set, at buyanydeal you can compare the best tv's and home cinema sets online based on the best technical specification, user review and price. Our database contains prices and products from web shops like <a href="https://go.urtrackinglink.com/aff_c?offer_id=120&aff_id=3321" title="Shop online at souq.com" target="_blank" rel="nofollow">souq.com</a>, <a href="https://go.urtrackinglink.com/aff_c?offer_id=547&aff_id=3321" title="Shop online at noon.com" target="_blank" rel="nofollow">noon.com</a> or even <a href="https://go.urtrackinglink.com/aff_c?offer_id=86&aff_id=3321" title="Shop online at wadi" target="_blank" rel="nofollow">jumbo</a>. Start selecting your best home cinema experience right away.</p>
            </div>
            <div class="col-12 col-md-6">
              <h3 class="h5">Mobiles and Tablets</h3>
              <p>Mobile phones and tablets are the most important accessories in our daily lives. Who doesn't have a nice smartphone now days? At buyanydeal you can compare the latest mobile phones online by looking at there specifications with camera, performance, display storage, reviews, ratings and more. If you are looking for the latest iPhone X, Samsung Galaxy, or even google pixel. Compare prices between all UAE online shops within one site.</p>
            </div>
            <div class="col-12 col-md-6">
              <h3 class="h5">Wearables</h3>
              <p>Find, compare and buy wearable tech solutions online at buyanydeal.  Whatever you are looking for, a smartwatch or a fitnesstracker you can find the latest models and trends on our website. Are you curious what the experience of other customers is, read the reviews of their purchases. We offer smartwatches from Fitbit, Apple, Samsung and many other brands at the lowest price in the market.</p>
            </div>
            <div class="col-12 col-md-6">
              <h3 class="h5">Games and Consoles</h3>
              <p>Are you game addicted as well? At buyanydeal we offer a wide range of products in the gaming and console category. At buyanydeal you can compare prices from shops in the UAE, but also we offer a wide range of offers from international online shops. Prices from PlayStation, Xbox and Nintendo are compared. From consoles, console accessories to games you can find it all at buyanydeal.</p>
            </div>
        </div>
    </div>
    <script type="application/ld+json">
        {
            "@context": "http://schema.org",
            "@type": "WebPage",
            "breadcrumb": "Home",
            "name": "Online shopping mall in UAE | Buyanydeal",
            "url": "https://buyanydeal.com/en",
            "description": "Compare prices in the online shopping mall in the UAE. Buyanydeal is makes online shopping fun and cheaper. Read reviews and compare.",
            "publisher": {
                "@type": "Organization",
                "name": "Buyanydeal"
            },
             "potentialAction": [
             {
                "@type": "SearchAction",
                "target": "https://buyanydeal.com/en/search?q={search_term_string}",
                "query-input": "required name=search_term_string"
             }]
        }
    </script>
    <script>
        $(document).ready(function(){
            $(".owl-slider").owlCarousel({
                autoHeight: true,
                autoplay: true,
                autoplayTimeout: 5000,
                autoplayHoverPause: true,
                items: 1,
                lazyLoad: true,
                loop: true,
                margin: 10,
                smartSpeed:1000
            });
            $(".owl-products").owlCarousel({
                autoHeight: false,
                autoWidth: false,
                items: 5,
                lazyLoad: true,
                lazyLoadEager: 1,
                loop: true,
                margin: 10,
                smartSpeed:200,
                responsive : {
                    1 : {
                        items: 2
                    },
                    768 : {
                        items: 4
                    },
                    1024 : {
                        items : 5
                    },
                    1200 : {
                        items : 6
                    }
                }
            });
        });
    </script>
@endsection
