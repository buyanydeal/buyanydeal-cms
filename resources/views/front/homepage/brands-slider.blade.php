<div class="owl-carousel owl-brands owl-theme">
    <a href="#">
        <picture>
            <source class="owl-lazy" data-srcset="https://placehold.it/350x200&text=brand-1">
            <img class="owl-lazy" data-src="https://placehold.it/350x250&text=1-fallback" alt="">
        </picture>
    </a>
    <a href="#">
        <picture>
            <source class="owl-lazy" data-srcset="https://placehold.it/350x200&text=brand-2">
            <img class="owl-lazy" data-src="https://placehold.it/350x250&text=3-fallback" alt="">
        </picture>
    </a>
    <a href="#">
        <picture>
            <source class="owl-lazy" data-srcset="https://placehold.it/350x200&text=brand-3">
            <img class="owl-lazy" data-src="https://placehold.it/350x250&text=3-fallback" alt="">
        </picture>
    </a>
    <a href="#">
        <picture>
            <source class="owl-lazy" data-srcset="https://placehold.it/350x200&text=brand-4">
            <img class="owl-lazy" data-src="https://placehold.it/350x250&text=3-fallback" alt="">
        </picture>
    </a>
    <a href="#">
        <picture>
            <source class="owl-lazy" data-srcset="https://placehold.it/350x200&text=brand-5">
            <img class="owl-lazy" data-src="https://placehold.it/350x250&text=3-fallback" alt="">
        </picture>
    </a>
    <a href="#">
        <picture>
            <source class="owl-lazy" data-srcset="https://placehold.it/350x200&text=brand-6">
            <img class="owl-lazy" data-src="https://placehold.it/350x250&text=3-fallback" alt="">
        </picture>
    </a>
    <a href="#">
        <picture>
            <source class="owl-lazy" data-srcset="https://placehold.it/350x200&text=brand-7">
            <img class="owl-lazy" data-src="https://placehold.it/350x250&text=3-fallback" alt="">
        </picture>
    </a>
    <a href="#">
        <picture>
            <source class="owl-lazy" data-srcset="https://placehold.it/350x200&text=brand-8">
            <img class="owl-lazy" data-src="https://placehold.it/350x250&text=3-fallback" alt="">
        </picture>
    </a>
    <a href="#">
        <picture>
            <source class="owl-lazy" data-srcset="https://placehold.it/350x200&text=brand-9">
            <img class="owl-lazy" data-src="https://placehold.it/350x250&text=3-fallback" alt="">
        </picture>
    </a>
    <a href="#">
        <picture>
            <source class="owl-lazy" data-srcset="https://placehold.it/350x200&text=brand-10">
            <img class="owl-lazy" data-src="https://placehold.it/350x250&text=3-fallback" alt="">
        </picture>
    </a>
</div>