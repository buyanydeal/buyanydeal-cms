<div class="product-card">
    @if(!$product->productshop->isEmpty())
        @foreach($product->productshop as $shop)
            @if($shop->special_price != '0.0')
                @php($percentage = round((($shop->original_price - $shop->special_price) / $shop->original_price) * 100))
                <div class="promotions">
                    <div class="label discount">{{ $percentage }}% cheaper</div>
                </div>
            @endif
        @endforeach
    @endif
    @if(isset($product->cover))
        <div class="product-img-wrapper">
            <a href="{{route('front.product.slug',['slug' => $product->slug])}}" title="Compare prices for {{$product->name}}" class="p-0">
                @if(strpos($product->cover, 'http') === 0)
                    <img class="img-fluid" src="{{ $product->cover }}" alt="Compare {{ $product->name }}" title="{{ $product->name }}">
                @else
                    <img src="{{ URL::asset("imagecache/thumbnail/$product->cover")}}" alt="{{ $product->name }}" title="Compare {{ $product->name }}">
                @endif
            </a>
        </div>
    @endif
    <div class="product-details">
        <p class="product-shop mb-2">{{ $product->brand->name }}</p>
        <p class="product-name">
            <a href="{{route('front.product.slug',['slug' => $product->slug])}}" title="Compare prices for {{$product->name}}" class="p-0">{{$product->name}}</a>
        </p>
        <div class="actions">
            <div class="price-box">
                @if(!$product->productshop->isEmpty())
                    @php( $shop_data = $product->productshop->where('lowest_price', $product->productshop->min('lowest_price'))->first())

                    @if($shop_data->special_price != '0.0')
                        <div class="old-price">AED {{ $shop_data->original_price }}</div>
                        <div class="price">AED {{ $shop_data->special_price }}</div>
                    @else
                        <div class="old-price lowest">Lowest price:</div>
                        <div class="price">AED {{ $shop_data->original_price }}</div>
                    @endif
                @endif
            </div>
        </div>
    </div>
</div>