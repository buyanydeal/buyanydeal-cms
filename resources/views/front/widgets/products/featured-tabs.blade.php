<div class="featured">
    <ul class="nav nav-tabs pl-0" id="myTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="women-tab" data-toggle="tab" href="#women" role="tab" aria-controls="women" aria-selected="true">Women Fashion</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="babykids-tab" data-toggle="tab" href="#babykids" role="tab" aria-controls="babykids" aria-selected="false">Baby & Kids</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="beauty-tab" data-toggle="tab" href="#beauty" role="tab" aria-controls="beauty" aria-selected="false">Beauty & Heath</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="books-tab" data-toggle="tab" href="#books" role="tab" aria-controls="books" aria-selected="false">Books</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="homegarden-tab" data-toggle="tab" href="#homegarden" role="tab" aria-controls="homegarden" aria-selected="false">Home & Garden</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="pets-tab" data-toggle="tab" href="#pets" role="tab" aria-controls="pets" aria-selected="false">Pet Supplies</a>
        </li>
    </ul>
    <div class="tab-content" id="myTabContent">
        <div class="tab-pane fade show active" id="women" role="tabpanel" aria-labelledby="women-tab">
            <div class="my-2">
                <div class="owl-carousel owl-products owl-theme">
                    @foreach($womenProducts as $product)
                        <div>
                            @include('front.widgets.products.product-block', [
                                'product' => $product,
                                'brand' => $product->brand->name,
                                'cover' => $product->cover,
                                'name' => $product->name,
                                'slug' => $product->slug
                            ])
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="text-center">
                <a href="#" class="btn btn-outline-primary rounded-0">See All Women Fashion</a>
            </div>
        </div>
        <div class="tab-pane fade" id="babykids" role="tabpanel" aria-labelledby="babykids-tab">
            <div class="my-2">Carousel</div>
            <div class="text-center">
                <a href="#" class="btn btn-outline-primary rounded-0">See All Baby & Kids</a>
            </div>
        </div>
        <div class="tab-pane fade" id="beauty" role="tabpanel" aria-labelledby="beauty-tab">
            <div class="my-2">Carousel</div>
            <div class="text-center">
                <a href="#" class="btn btn-outline-primary rounded-0">See All Beauty & Health</a>
            </div>
        </div>
        <div class="tab-pane fade" id="books" role="tabpanel" aria-labelledby="books-tab">
            <div class="my-2">Carousel</div>
            <div class="text-center">
                <a href="#" class="btn btn-outline-primary rounded-0">See All Books</a>
            </div>
        </div>
        <div class="tab-pane fade" id="homegarden" role="tabpanel" aria-labelledby="homegarden-tab">
            <div class="my-2">Carousel</div>
            <div class="text-center">
                <a href="#" class="btn btn-outline-primary rounded-0">See All Home & Garden</a>
            </div>
        </div>
        <div class="tab-pane fade" id="pets" role="tabpanel" aria-labelledby="pets-tab">
            <div class="my-2">Carousel</div>
            <div class="text-center">
                <a href="#" class="btn btn-outline-primary rounded-0">See All Pets Supplies</a>
            </div>
        </div>
    </div>
</div>

<script>
    require([
        'jquery',
        'owl.carousel'

    ], function ($) {
        $(".owl-products").owlCarousel({
            autoHeight: false,
            autoWidth: false,
            items: 5,
            lazyLoad: true,
            lazyLoadEager: 1,
            loop: true,
            margin: 10,
            smartSpeed:1000,
            responsive : {
                360 : {
                    items: 2
                },
                768 : {
                    items: 4
                },
                1024 : {
                    items : 5
                },
                1200 : {
                    items : 5
                }
            }
        });
    });
</script>