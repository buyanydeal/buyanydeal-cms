<div class="box box-product mb-4">
    <div class="box-title clearfix mb-3">
        <h2 class="h4 d-inline-block">{{ $title }}</h2>
        <a href="{{ $route }}" class="btn btn-outline-primary btn-sm float-right" title="{{ $title }}">Compare all</a>
    </div>
    <div class="block-content">
        <div class="owl-carousel owl-products owl-theme" role="list">
            @foreach($products as $product)
                <div>
                    @include('front.widgets.products.product-block', [
                        'product' => $product,
                        'brand' => $product->brand->name,
                        'cover' => $product->cover,
                        'name' => $product->name,
                        'slug' => $product->slug
                    ])
                </div>
            @endforeach
        </div>
    </div>
</div>