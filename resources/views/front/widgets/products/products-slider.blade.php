<div class="recommendations my-3">
    <h2 class="h5">Top 10 Products</h2>
    <div class="my-2">
        <div class="owl-carousel owl-top10-prod owl-theme">
            <div>
                @include('front.shared.products.product-block')
            </div>
            <div>
                @include('front.shared.products.product-block')
            </div>
            <div>
                @include('front.shared.products.product-block')
            </div>
            <div>
                @include('front.shared.products.product-block')
            </div>
            <div>
                @include('front.shared.products.product-block')
            </div>
            <div>
                @include('front.shared.products.product-block')
            </div>
            <div>
                @include('front.shared.products.product-block')
            </div>
            <div>
                @include('front.shared.products.product-block')
            </div>
            <div>
                @include('front.shared.products.product-block')
            </div>
            <div>
                @include('front.shared.products.product-block')
            </div>
        </div>
    </div>
</div>

<script>
    require([
        'jquery',
        'owl.carousel'

    ], function ($) {
        $(".owl-top10-prod").owlCarousel({
            autoHeight: false,
            autoWidth: false,
            items: 5,
            lazyLoad: true,
            lazyLoadEager: 1,
            loop: true,
            margin: 10,
            smartSpeed:1000,
            responsive : {
                360 : {
                    items: 2
                },
                768 : {
                    items: 4
                },
                1024 : {
                    items : 5
                },
                1200 : {
                    items : 5
                }
            }
        });
    });
</script>