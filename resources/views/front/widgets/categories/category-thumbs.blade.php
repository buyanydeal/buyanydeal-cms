<div class="categories my-3">
    <h2 class="h5">Top 10 Categories</h2>
    <div class="owl-carousel owl-category owl-theme">
        <div class="category-card">
            <div class="category-img-wrapper">
                <a href="#" title="">
                    <img class="img-responsive" src="https://buyanydeal.com/sites/default/files/styles/category_thumbs/public/beauti.jpg?itok=jixcNj50" alt="">
                </a>
            </div>
            <div class="category-details">
                <h2 class="category-name">
                    <a href="/beauty-health">Beauty &amp; Health</a>
                </h2>
            </div>
        </div>
    </div>
    <div class="text-center">
        <a href="#" class="btn btn-outline-primary rounded-0">See All Categories</a>
    </div>
</div>

<script>
    require([
        'jquery',
        'owl.carousel'

    ], function ($) {
        $(".owl-category").owlCarousel({
            autoHeight: true,
            autoplay: true,
            autoplayTimeout: 3000,
            autoplayHoverPause: true,
            items: 5,
            lazyLoad: true,
            lazyLoadEager: 1,
            loop: true,
            margin: 10,
            smartSpeed:1000,
            responsive : {
                360 : {
                    items: 2
                },
                768 : {
                    items: 4
                },
                1024 : {
                    items : 5
                },
                1200 : {
                    items : 5
                }
            }
        });
    });
</script>