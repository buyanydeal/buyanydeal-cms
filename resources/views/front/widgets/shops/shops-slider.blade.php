<div class="shops my-3">
    <h2 class="h5">Top 10 Shops</h2>
    <div class="owl-carousel owl-shops owl-theme">
        <a href="#">
            <picture>
                <source class="owl-lazy" data-srcset="https://placehold.it/350x200&text=brand-1">
                <img class="owl-lazy" data-src="https://placehold.it/350x250&text=1-fallback" alt="">
            </picture>
        </a>
        <a href="#">
            <picture>
                <source class="owl-lazy" data-srcset="https://placehold.it/350x200&text=brand-2">
                <img class="owl-lazy" data-src="https://placehold.it/350x250&text=3-fallback" alt="">
            </picture>
        </a>
        <a href="#">
            <picture>
                <source class="owl-lazy" data-srcset="https://placehold.it/350x200&text=brand-3">
                <img class="owl-lazy" data-src="https://placehold.it/350x250&text=3-fallback" alt="">
            </picture>
        </a>
        <a href="#">
            <picture>
                <source class="owl-lazy" data-srcset="https://placehold.it/350x200&text=brand-4">
                <img class="owl-lazy" data-src="https://placehold.it/350x250&text=3-fallback" alt="">
            </picture>
        </a>
        <a href="#">
            <picture>
                <source class="owl-lazy" data-srcset="https://placehold.it/350x200&text=brand-5">
                <img class="owl-lazy" data-src="https://placehold.it/350x250&text=3-fallback" alt="">
            </picture>
        </a>
        <a href="#">
            <picture>
                <source class="owl-lazy" data-srcset="https://placehold.it/350x200&text=brand-6">
                <img class="owl-lazy" data-src="https://placehold.it/350x250&text=3-fallback" alt="">
            </picture>
        </a>
        <a href="#">
            <picture>
                <source class="owl-lazy" data-srcset="https://placehold.it/350x200&text=brand-7">
                <img class="owl-lazy" data-src="https://placehold.it/350x250&text=3-fallback" alt="">
            </picture>
        </a>
        <a href="#">
            <picture>
                <source class="owl-lazy" data-srcset="https://placehold.it/350x200&text=brand-8">
                <img class="owl-lazy" data-src="https://placehold.it/350x250&text=3-fallback" alt="">
            </picture>
        </a>
        <a href="#">
            <picture>
                <source class="owl-lazy" data-srcset="https://placehold.it/350x200&text=brand-9">
                <img class="owl-lazy" data-src="https://placehold.it/350x250&text=3-fallback" alt="">
            </picture>
        </a>
        <a href="#">
            <picture>
                <source class="owl-lazy" data-srcset="https://placehold.it/350x200&text=brand-10">
                <img class="owl-lazy" data-src="https://placehold.it/350x250&text=3-fallback" alt="">
            </picture>
        </a>
    </div>
</div>

<script>
    require([
        'jquery',
        'owl.carousel'

    ], function ($) {
        $(".owl-shops").owlCarousel({
            autoHeight: true,
            autoplay: true,
            autoplayTimeout: 3000,
            autoplayHoverPause: true,
            items: 5,
            lazyLoad: true,
            lazyLoadEager: 1,
            loop: true,
            margin: 10,
            smartSpeed:1000,
            responsive : {
                360 : {
                    items: 3
                },
                768 : {
                    items: 5
                },
                1024 : {
                    items : 5
                },
                1200 : {
                    items : 5
                }
            }
        });
    });
</script>