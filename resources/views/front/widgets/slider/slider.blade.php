<div class="mb-4">
    <div class="owl-carousel owl-slider owl-theme">
        @foreach($slider as $item)
            <a href="{{ $item->url }}" title="{{ $item->name }}">
                <picture>
                    <source class="owl-lazy" media="(min-width: 650px)" data-srcset="{{ asset("storage/$item->image_desktop") }}" srcset="{{ asset("storage/$item->image_desktop") }}">
                    <source class="owl-lazy" media="(min-width: 1px)" data-srcset="{{ asset("storage/$item->image_mobile") }}" srcset="{{ asset("storage/$item->image_mobile") }}">

                    <img class="owl-lazy" data-src="{{ asset("storage/$item->image_desktop") }}" src="{{ asset("storage/$item->image_desktop") }}" alt="{{ $item->name }}" title="{{ $item->name }} Today">
                </picture>
            </a>
        @endforeach
    </div>
</div>

