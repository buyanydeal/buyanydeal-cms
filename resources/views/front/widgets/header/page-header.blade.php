@if($cover && $description)
<div class="category-header d-none d-md-block d-flex mb-3">
    <div class="row align-items-center">
        <div class="col-md-6">
            <img class="img-fluid" src="{{ asset("storage/$cover") }}" alt="{{ $name }} Product Comparison" title="{{ $name }}">
        </div>
        <div class="col-md-6 text-center">
            {!! $description !!}
        </div>
    </div>
</div>
@endif