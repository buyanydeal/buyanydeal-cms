<?php

return [

    'brands'        => 'Brands',
    'catalog'       => 'Catalog',
    'categories'    => 'Categories',
    'cms'           => 'CMS',
    'dashboard'     => 'Dashboard',
    'products'      => 'Products',
    'shops'         => 'Shops',
    'sliders'       => 'Sliders',
    'system'        => 'System',
    'users'         => 'Users',

];