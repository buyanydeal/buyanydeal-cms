<?php

return [

    'add-users'     => 'Add User',
    'email'         => 'E-mail',
    'id'            => 'ID',
    'manage-users'  => 'Manage Users',
    'name'          => 'Name',

];