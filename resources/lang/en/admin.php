<?php

return [

    'active'                => 'Active',
    'add-categories'        => 'Add Category',
    'brands'                => 'Brands',
    'dashboard'             => 'Dashboard',
    'categories'            => 'Categories',
    'catalog'               => 'Catalog',
    'disabled'              => 'Disabled',
    'enabled'               => 'Enabled',
    'id'                    => 'ID',
    'manage-categories'     => 'Manage Categories',
    'name'                  => 'Name',
    'no-parent'             => 'No Parent',
    'parent-category'       => 'Parent Category',
    'shops'                 => 'Shops',
    'slug'                  => 'URL Key',
    'status'                => 'Status',

];