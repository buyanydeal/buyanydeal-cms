<?php

return [

    'brands' => 'Brands',
    'browse-category' => 'Browse by category',
    'close' => 'Close',
    'compare-brand' => 'Compare Brand Prices',
    'compare-stores' => 'Compare Stores',
    'logo-alt' => 'Buyanydeal Price Comparison UAE',
    'menu' => 'Menu',
    'page-title' => 'Your online shopping mall in the UAE',
    'search' => 'Search',
    'search-placeholder' => 'Try Iphone',
    'select-category' => 'Select a category',
    'stores' => 'Stores',

];